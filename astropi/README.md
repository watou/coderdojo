# Mission Space Lab

## Proposal from CoderDojo Clonakilty

### WorkSpace Comfort Monitor

The code will read the sensors on the Sense HAT every five seconds (2,160 samples in three hours) and measure light from the camera, store them timestamped\* in a CSV file, and compare the sensor values to ranges that are known to support a comfortable work environment for humans.  Is there enough air pressure to breathe comfortably?  If the temperature too hot or cold?  Is the air too dry or wet? Is there enough light to read the instruments and do work? 

The LEDs will show status of all readings to make sure they are within the acceptable range for humans, using colours and funny animated icons to indicate how "comfortable" they are.  When the team gets the data back from the ISS, it will compare the recorded times to public data to know exactly where in orbit the ISS was when the code was run.  The team will make a report that describes how different the space outside the module was than inside the module, and will have an infographic that shows the range of comfort for humans, compared to inside and outside the module.

\_\_\_\_\_\_\_\_

\* We can assume that when the code is running, the clock on the Raspberry Pi will be roughly accurate, since it must be connected to a network for it to be remotely controlled from Earth.  Since it's connected to a network, it's reasonable to assume that the Pi will get the current time from a network time server.


## Acceptance

We received the very happy news that we can compete!


> Dear CoderDojo Clonakilty,

> Phase 2 of the European Astro Pi Challenge - Mission Space Lab has just started! We are very enthusiastic about it and we hope that you too.
 
> In this second phase, which will take place between 7 November 2017 and 7 February 2018, you will have to write the computer code with which the Astro Pi need to be programmed in order to have your experiment run on the International Space Station. You can find below some personalised feedback regarding your proposal:
 
> _Great idea, one thing to be aware of is the thermal leak between the Raspberry Pi CPU and the temperature sensors on the Sense HAT. This is a known hardware flaw, in previous competitions some teams have tried to calibrate out the error.Also be aware that the Pi on board the ISS is an old B+ circa 2014, and you'll get a Pi 3B in your kit which is a lot faster (and warmer)._
 
> We would also like to remind you about some restrictions related to the different missions:
 
> ...

> ‘Life in Space’
> Astro Pi Ed has only a visible camera, however it is not possible to save photographs taken.
> Ed will not be pointing at a window, so observing space is not a possibility.
 
> Finally, teams should also take in consideration that:

> * The Raspberry Pi’s are fixed in place and unable to move or rotate.
> * Sensors are only capable of measuring internal ISS conditions.
> * It is not possible to communicate with, or send live information to, the Raspberry Pi.
> * You cannot specify where on Earth the ISS will pass during the time your code will run. You will only know this after you receive your data!
 
> The ESA Astro Pi team asks that you please refer again to the [competition guidelines](http://esamultimedia.esa.int/docs/edu/European_Astro_Pi_Challenge_Mission_Space_Lab_guidelines.pdf) and [code rules](http://esamultimedia.esa.int/docs/edu/Mission_Space_Lab_coding_rules.pdf) as you begin writing your code. Read them carefully and if you have any questions please send an email to astropi@esa.int. For technical questions regarding Astro Pi please check the [FAQ section](https://astro-pi.org/missions/space-lab/faq/) in the Astro Pi website as well as the Astro Pi forums. If you can’t find the answer you’re looking for, please send an email to enquiries@astro-pi.org.

> We remind you that you will be able to submit your code between **7 January 2018 and 7 February 2018** [here](https://astro-pi.org/missions/space-lab/).
 
> Good luck!

> Kind regards,

> The ESA Astro Pi Team


## Remaining To Do Items (updated Mon 22 Jan)

1. **Research scientific basis for comfortable pressure range, add to programme.**  I feel we've done enough on temperature, humidity and light, but air pressure is outstanding as a subject for understanding the science, and choosing meaningful high and low values. _(Complete - Kate is documenting her research)_

1. Review and improve all the icons, make them as clear as possible, and hopefully a little bit funny. _(Complete - Updated icons will appear in next version of code)_

1. **Replace numeric pixel (r,g,b) values with short variable names.**  We want to have the icons sort-of visible in the source code, similar to how the pressure icons are done. _(Complete)_

1. **Move the last frame to the first frame, have it linger long enough to be understood by the viewer.**  Greg suggested that the "T =", "P +", etc., frames of animations be moved to the first frame, but have it linger as it currently is as the last frame.  So instead of watching the animation and then seeing what it meant, start with what it means and then do the animation. _(Complete)_

1. **Document the code similar to how it's is done in watchdog.py.** The goal is to have nothing about the programme be mysterious, "magic" or otherwise not clearly explained.  This applies to both organisation and functioning of the code. _(TBC - Wendy, James and Kate to complete once code is fully functional)_

1. **Identify the best way to have the sensors read close to five seconds apart**. _(John updated in his last program)_

1. **Write the log to a file instead of stdout**. The name of the file should contain a timestamp or serial index so we have one file per run _(John updated in his last program -  James to test output)_

1. **Run the programme for 3 hours, report file size.** The survey wants to know the total amount of space in megabytes, that a typical log file consumes in three hours _(TBC -  the goal is to have this completed by Saturday 27_01)_

1. **Complete the survey questions [here](https://astro-pi.org/missions/space-lab/submit-code/)**. _(TBC - on launch day 31-01)_


## Questions

Team members should explore these questions to have a good understanding of what we are trying to measure and how a person is affected.  The project will be most successful if we have a solid understanding of what we are measuring and why.

### Python Programming Language

#### Links

* [learnpython.org](https://www.learnpython.org/)


### Making 8x8 Icons

* [Youtube video: How To Pixel Art Tutorials - Draw 8x8 Pixel Character](https://www.youtube.com/watch?v=m5PHiJjt270)
* [Google search: 8x8 pixel animation](https://www.google.ie/search?biw=1403&bih=736&tbm=isch&sa=1&ei=Se0FWvL4L8iOgAbbqpKQCA&q=8x8+pixel+animation&oq=8x8+pixel+animation&gs_l=psy-ab.3..0i24k1.21953.22188.0.22534.3.3.0.0.0.0.77.208.3.3.0....0...1.1.64.psy-ab..0.1.77....0.IUVGUKBeVhY#imgrc=TFTcRDjMkQ440M:
)


### Air Pressure

1. Why do people need to breathe?
1. What gas is important for people to breathe?
1. What is air composed of?
1. Why are there masks that could drop down over seats on an airplane?
1. What is "hypoxia"?
1. How high is Mt. Everest in metres?  
1. How do people who climb to the top breathe there?
1. At what temperature does water boil on your hob at home?
1. At what temperature does water boil at the top of Mt. Everest?
1. What are some different units of measure used to measure air pressure?
1. What is the definition of "one atmosphere" of air pressure?  
1. What fraction of one atmoshpere is there at the top of Mt. Everest?
1. How many millibars are there in one atmosphere?
1. What is the air pressure in space?
1. What is the air pressure on the ISS?
1. What is the breathable air on the ISS composed of?
1. What is "the bends" and who usually suffers from it?

![Pressure and Oxygen](images/atmchart.jpg)
![How Long to Breathe](images/atmchart2a.jpg)

#### Notes

At about 12,000 m (40,000 feet) above sea level, the pressure is low enough that most humans will get hypoxia while breathing pure oxygen. This sets a limit of about 18,800 Pa. Of course, some humans have reached the summit of Mt Everest, where the partial pressure of oxygen is about 6,000 Pa (atmospheric pressure is about 30,000 Pa), without supplemental oxygen(they may also have been suffering from chronic hypoxia). At human body temperature (about 37C), water will boil at about 6,330 Pa. 

Probably, at a pressure of about 10,000 Pa, even in a pure oxygen atmosphere, most people would die from anoxia, well above the ambient pressure where bodily water to start to boil.

> What if the partial pressure of oxygen is about the same, or double, but there's less oxygen molecules per volume?

> Suppose you have a mixture of gases that is 40% oxygen at a pressure of 50 kPa and the combined density of all the gases is 50 grams/m^3.

Within the range of physiological gas pressures, the partial pressure of any one gas is independent of the other gases that may or may not be present. If the partial pressure of oxygen is the same, then the number of O2 molecules per volume is the same.

In air (sea level) ~1 bar or 100 kPa, Oxygen has a partial pressure of ~ 21kPa. The addition of extra gas does not change the partial pressure of oxygen (the parameter that is important for oxygen uptake) and one can breath just fine with no extra gases or several atm of extra gas. (The addition of higher pressure inert gases can cause problems with such gases being absorbed).



#### Links

* [Atmospheric Pressure](https://www.nationalgeographic.org/encyclopedia/atmospheric-pressure/)
* [Air Pressure and Human Survival](https://forum.cosmoquest.org/showthread.php?121894-Air-Pressure-and-Human-Survival)
* [Effect of Barometric Pressure on Humans](https://healthfully.com/effects-barometric-pressure-humans-5410462.html)


### Temperature

1. What different units are used to measure temperature?
1. What temperature is it in your house?
1. What temperature is it in space?
1. How hot is it on the surface of the sun?

#### Notes

The Sense HAT temperature sensor reads the temperature where it is installed, not the temperature in the air outside the Raspberry Pi case.  This is unfortunate, since we want to know the temperature out in the air where people are, not the temperature inside the case on the surface of the Sense HAT board, which is always going to be hotter.  Fortunately, there have been efforts to find a way to compensate for the difference, with one example approach [here](https://github.com/initialstate/wunderground-sensehat/wiki/Part-3.-Sense-HAT-Temperature-Correction).

#### Links

* [Sense HAT Temperature Correction](https://github.com/initialstate/wunderground-sensehat/wiki/Part-3.-Sense-HAT-Temperature-Correction)
* [Astro Pi Temperature Sensor Reading](https://www.raspberrypi.org/forums/viewtopic.php?f=104&t=111457&sid=4a2cceb64c18165ab7d6aff87a07c1e4&start=25#p908752)

### Humidity

1. What does humidity measure in the air?
1. What is the definition of "relative humidity?"
1. What is the definition of "dew point temperature?"
1. What is the definition of "vapour pressure?"
1. Why do humans prefer a certain amount of humidity?

#### Notes

The dew point temperature is very important to human comfort.  The Magnus formula can be used to find the dew point temperature if you know the both ambient temperature and relative humidity.  The humidity sensor on the Sense HAT will tell us both of those values.

We know that the temperature sensors in the Sense HAT are not reading the temperature of the air outside the Raspberry Pi case, but instead are reading a higher temperature due to the proximity of heat-generating components and the insulating effect of the case that encloses the electronics.  But since the humidity sensor also reads temperature, we can take those two sensor readings and use them together with the Magnus formula to compute the dew point temperature and consider it sufficiently reliable.  Then we can use the information [here](https://www.raspberrypi.org/learning/sensing-the-weather/lesson-7/worksheet/) to inform what is considered a comfortable dew point temperature. 

| Dew Point Temperature (°C)  |  Human Comfort Level |
|----------------------|----------|
| 0  |  A bit dry for some |
| 12 | Very comfortable |
| 16 | Comfortable |
| 18 | Upper edge of comfortable |
| 21 | Somewhat uncomfortable |
| 24 |  Quite uncomfortable |
| 26 |  Extremely uncomfortable |
| 27 | Severely high to deadly |


The Magnus formula (in Python):

```python
dew_point = ((humidity / 100) ** 0.125) * (112 + 0.9 * temperature) + (0.1 * temperature) – 112
```

> The dew point is the temperature at which the water vapour in the air will condense to form liquid dew. It's important to be able to measure the dew point in lots of situations. If the dew point temperature is reached indoors, this will result in condensation and cause mould and mildew to grow, affecting people's health. If the dew point is negative (sometimes called the frost point) and the air temperature equals the dew point, this will result in the water vapour freezing and causing frost.
So we will convert relative humidity to absolute humidity using the temperature read from the humidity sensor, and then use the calibrated temperature to convert the absolute humidity back to relative humidity.

#### Links

* [Wikipedia: relative humidity](https://en.wikipedia.org/wiki/Relative_humidity)
* [Wikipedia: dew point: Relationship to human comfort](https://en.wikipedia.org/wiki/Dew_point#Relationship_to_human_comfort)
* [Magnus formula](https://www.raspberrypi.org/learning/sensing-the-weather/lesson-7/worksheet/)


### Light

1. What unit of measure is used for the amount of light in a room?
1. What is complete darkness in those units?
1. In those units, what is considered the minimum amount of light for people to work in?


####  Notes

We will use the Raspberry Pi Camera Module to measure the amount of light in the workspace.  Cameras are not usually used for this purpose, but they have capabilities that will result in a sufficiently useful measurement.  Modern cameras automatically adjust for the amount of light entering the lens, so the resulting photo has enough, but not too much, light to be clear.  The Pi Camera can adjust how long its sensor is exposed to light (exposure time), and how much to amplify the light it gathered (gain).  From our Python code, we can query the camera for the exposure time and gain it is using, to see how the camera is compensating for lighting conditions in the scene in front of it.  We can then use the work performed by the camera module to calculate light.

From the [Wikipedia page: Lux](https://en.wikipedia.org/wiki/Lux):

| Illuminance (lux) | Surfaces illuminated by |
|-------------------|-------------------------|
| 0.0001   | Moonless, overcast night sky (starlight) |
| 0.002    | Moonless clear night sky with airglow |
| 0.05–0.3 | Full moon on a clear night |
| 3.4      | Dark limit of civil twilight under a clear sky |
| 20–50    | Public areas with dark surroundings |
| 50       | Family living room lights (Australia, 1998) |
| 80       | Office building hallway/toilet lighting |
| 100      | Very dark overcast day |
| 320–500  | Office lighting |
| 400      | Sunrise or sunset on a clear day. |
| 1000     | Overcast day; typical TV studio lighting |
| 10,000–25,000 | Full daylight (not direct sun) |
| 32,000–100,000 | Direct sunlight |



##### Exposure Time

The sensors in digital cameras, much like old-fashioned film in analog cameras, are sensitive to light, and the longer they are exposed to light, the brighter the image gets.  A white, washed out photo on a bright, sunny day at the beach is considered over-exposed, because the sensor or film was exposed to that bright light longer than necessary.  The Raspberry Pi camera can automatically adjust how long it thinks its sensor should be exposed to light, depending on how much light is available.  We will use this information in our code.

##### Gain

"Gain" multiplies a sensor value to make it brighter.  A gain of 1 means the sensed value is good as is (since multiplying anything by 1 doesn't change it). In the case of the camera, that means the camera thinks there is enough light in the picture already.  But if the gain of 2 is used, then the picture will be made twice as bright as what the sensors could see.

The camera has two independent gains it applies: `analog_gain` happens within the sensor itself.  If that is still too dark, a `digital_gain` is applied after that to brighten up the image even further (but not as well as the `analog_gain` did).  Together these are considered the overall gain the camera applied in order to make a good photo.  Like exposure time, the Raspberry Pi Camera Module can automatically adjust gain in order to produce a good photo, so all we have to do is ask the camera what its gain is to learn if there is enough light in the room.  In traditional photography, something called "ISO units" are used for measuring "film speed" and are easy to convert to and from gain.

At [this link](http://www.conservationphysics.org/lightmtr/luxmtr1.php), we see that there is a rough way of using a camera to calculate the amount of light in the scene in front of the camera.  We also need to know certain information about the specific camera, the exposure time, and ISO units.  There have been four official camera modules for the Raspberry Pi so far, two work well in normal daylight, and two are intended to work at night.  We are using one of the daylight models, but we don't know for certain which one, either V1 or V2.  So our code needs to adjust for the physical differences between these two camera modules.

##### Code Example

Set up the camera so it is free to automatically choose its own exposure times and gains based on an average of the light hitting its sensor:

```python
# get ready to talk to the Pi Camera
cam = PiCamera()

# Set up the camera so it is free to choose its exposure times and gains:
cam.exposure_mode = 'auto'

# Take the average of the image pixels to automatically adjust exposure time and gain
cam.meter_mode = 'average'

# Let the camera module automatically set its gain
cam.iso = 0
```

Determine Lux from the V1 or V2 camera module:

```python
        # compensate for old vs. new camera modules
        if cam.revision == 'ov5647':
            # This is a V1 camera
            aperture = 2.9
            iso_conversion = 1.0
        else:
            aperture = 2.0
            iso_conversion = 1.84

        # Add the analog and digital gains for overall gain
        gain = cam.analog_gain + cam.digital_gain

        # ISO is 100x the gain, first converted depending on camera model
        iso = (gain / iso_conversion) * 100

        # we need exposure time in seconds, not microseconds
        speed = cam.exposure_speed / 1000000.0

        # Luminance in candela per square metre is:
        # 12.4 multiplied by the lens aperture squared
        # divided by
        # exposure time in seconds multiplied by the film speed in ISO units
        cdsqm = ((12.4 * (aperture ** 2))) / (speed * iso)

        # Lux is candela per square metre multiplied by Pi
        self.light = cdsqm * pi
```

#### Links

* [Wikipedia page: Lux](https://en.wikipedia.org/wiki/Lux)
* [Using a Camera as a Lux Meter](http://www.conservationphysics.org/lightmtr/luxmtr1.php)
* [Measuring light levels with camera](https://www.raspberrypi.org/forums/viewtopic.php?p=774618)
* [Controlling analog gain](https://www.raspberrypi.org/forums/viewtopic.php?t=156754)
* [Lighting Assessment in the Workplace (PDF)](http://www.labour.gov.hk/eng/public/oh/Lighting.pdf)
* [Lighting of indoor workplaces](https://www.fagerhult.com/knowledge-hub/EN-12464-1/Lighting-of-indoor-workplaces/)
* [Recommended luminance conditions](https://www.fagerhult.com/knowledge-hub/EN-12464-1/Recommended-luminance-conditions/)
* [Pi Camera Shutter Speed](https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.shutter_speed)

### SPOILER! Answers so far

Adriana: air pressure on the ISS is 101.3 kpa (14.7 psi)

comfortable humidity is between 30% and 50% relative humidity

temperature is too hot for a workplace if it's over 27 degrees Celsius
too cold?  ___

light minimum 500 lux -- check this!
too bright? ____
