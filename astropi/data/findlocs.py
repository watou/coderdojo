#
# Find out where the ISS was in relation to earth's surface
# for every data sample we gathered.  Using information about
# where the ISS was shortly before our code ran and the exact
# times our data were gathered, the PyEphem python module can
# compute the latitude and longitude of the point on the earth's
# surface below the ISS, how many metres above sea level the
# ISS was flying by, and even whether the ISS was in earth's
# shadow.
#
# Inspired by the Astro Pi training webinar recorded here:
#
# https://www.youtube.com/watch?v=WYjaAZcWIPg&feature=youtu.be&t=2h9m12s

import ephem            # sudo pip3 install ephem
import csv
import dateutil.parser  # sudo pip3 install python-dateutil
import math

# We want the TLE data for the ISS right before our code was run
# in order to get the most accurate location information.
# Create an account, login and retrieve the TLE data for the ISS
# with this URL:
#
# https://www.space-track.org/basicspacedata/query/class/tle/EPOCH/2018-04-26--2018-04-27/NORAD_CAT_ID/25544/orderby/TLE_LINE1%20ASC/format/tle

name = "ISS (ZARYA)"             
line1 = "1 25544U 98067A   18116.07049948  .00001993  00000-0  37290-4 0  9995"
line2 = "2 25544  51.6426 278.5198 0002698  19.8603  86.7813 15.54162986110415"

iss = ephem.readtle(name, line1, line2)

# Read the CSV our code created on the ISS and use each row's
# timestamp to compute the ISS location, and write a new CSV
# file with the new rows of information appended to the end.

def locate_iss(filename):
    with open(filename, newline='') as incsv:
        reader = csv.DictReader(incsv)
        with open(filename + '.loc.csv', 'w', newline='') as outcsv:
            writer = None # wait to create it until after we know the header
            for row in reader:
                # write the CSV file to the output file
                if writer == None:
                    fieldnames = list(reader.fieldnames)
                    fieldnames.extend(['sublat', 'sublong', 'elevation', 'eclipsed'])
                    writer = csv.DictWriter(outcsv, fieldnames=fieldnames)
                    writer.writeheader()
                    
                # read the ISO8601 date format
                dt = dateutil.parser.parse(row['time'])
                # compute the position of the ISS at that time
                iss.compute(dt)
                
                # add the computed data to row
                # add location as decimal degrees as opposed to non-decimal
                # degrees or radians
                row['sublat'] = float(repr(iss.sublat)) * (180/math.pi)
                row['sublong'] = float(repr(iss.sublong)) * (180/math.pi)
                row['elevation'] = iss.elevation
                row['eclipsed'] = iss.eclipsed

                # save the augmented row to the output CSV file
                writer.writerow(row)

# We were sent back two separate data files from the ISS

locate_iss('CoderDojo Clonakilty_comfort-20180426-163426.csv')
locate_iss('CoderDojo Clonakilty_comfort-20180427-080221.csv')

# We show now have these two files:
# CoderDojo Clonakilty_comfort-20180426-163426.csv.loc.csv
# CoderDojo Clonakilty_comfort-20180427-080221.csv.loc.csv
