#
#                                       Teolaí
#
#                         (Irish word for cozy/comfortable)
#
#      Credits:
#
#      Development Team: Greg Robey (16), James Robey (13), John Deegan (11),
#                        Adriana Cullinane (11), Darragh Walsh (11) and
#                        Tom O'Donovan (11)
#
#      Mentors: John Cocula and Kate O'Donovan
#
#      Teolaí (pronounced CHO lee) is the comfort awareness module from the
#      Clonakilty CoderDojo development team.
#      Teolaí uses the Raspberry Pi and its Sense HAT to read the environment
#      in the ISS and determine if the environment is within an acceptable
#      range for humans to live and work.
#
#      The Teolaí team researched acceptable ranges for the following
#      comfort values: Temperature, Humidity, Light and Pressure.
#
#      Approximately every 5 seconds, Teolaí will assess the environment and
#      record the values. These values are saved to a file for later analysis.
#
#      In addition, Teolaí will display a series of icons to the LED matrix to
#      communicate to anyone looking at the module what the current conditions
#      are.
#

from time import sleep, time    #
import datetime                 #
import os                       #
from math import pi             #  These are all the modules required to run
from sense_hat import SenseHat  #  the code
from picamera import PiCamera   #
import sys                      #

# CONSTANT VALUES

SENSOR_READ_INTERVAL = 5 # Read sensors no more often than 5 seconds
FRAMETIME = 0.3 # Time between frames is three tenths of a second
LINGERTIME = 1 # Linger on an important frame for one second

# COMFORT RANGES

TEMP_LOW = 17.5      #
TEMP_HIGH = 29.5     #
DEWPOINT_LOW = 0     #
DEWPOINT_HIGH = 18   #  Any number greater or less than these
LIGHT_LOW = 500      #  maximums/minimums is undesirable
LIGHT_HIGH = 1075    #
PRESSURE_LOW = 750   #
PRESSURE_HIGH = 1020 #

# TEMP_LOW is based upon Irish constitutional law for minimum temperature
# in an Irish working environment.
# SOURCE:
# http://www.irishstatutebook.ie/eli/2007/si/299/made/en/print#article7
#
# TEMP_HIGH is based upon a study of optimal working conditions from a 2006
# Helsinki University of Technology study.  The study identified an ideal
# temperature of 23 degrees Celsius.  We calculated the High assuming optimal
# temp was the mean.
# SOURCE:
# https://indoor.lbl.gov/sites/all/files/lbnl-60946.pdf
#
# DEWPOINT_LOW and DEWPOINT_HIGH values are based upon a RaspberryPi tutorial
# SOURCE:
# https://www.raspberrypi.org/learning/sensing-the-weather/lesson-7/worksheet/
#
# LIGHT_LOW is based upon requirements defined by the UK Health Service
# Executive and further defined by The Chartered Institution of Building
# Services Engineers (CIBSE).  They have defined the minimum level of Lux for
# engineers and other scientific workers.
#
# LIGHT_HIGH is not defined in industry. However there is a table of common
# natural light Lux levels.  We assumed working in light about an overcast day
# would be too taxing.
# SOURCE:
# https://www.engineeringtoolbox.com/light-level-rooms-d_708.html
#
# PRESSURE_LOW and PRESSURE_HIGH are based upon Requirements set by the FAA
# for cabin pressure within aircraft both on the ground and at flying
# altitude.
# SOURCE:
# https://www.ncbi.nlm.nih.gov/books/NBK207472/

# COlOURS

_ = (0,0,0) #BLACK
r = (237, 0, 6) #RED
o = (231, 140, 0) #ORANGE
g = (0, 192, 24) #GREEN
w = (255, 255, 255) #WHITE
b = (67,110,238) #BLUE
y = (255,255,0) #YELLOW
B = (206, 232, 230)#LIGHTBLUE
O = (255,165,0) #ORANGE2
p = (255,160,122) # PEACH
e = (127,127,127) # GREY


# ANIMATIONS

LOW = 0    # Animation for icon LOW will be displayed if sensors read low
NORMAL = 1 # Animation for icon NORMAL will be displayed if sensors read normal
HIGH = 2   # Animation for icon HIGH will be displayed if sensors read high

pressure_animations = [
    [
        # Pressure low
        # This set of icons uses a deflating balloon to represent low pressure

        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, r, r, _, r, r, r, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, r, r, r, r, _, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, _, r, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, r, r, _, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, _, r, r, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, r, r, _, _, _, _,
         _, r, r, r, r, _, _, _,
         r, _, _, _, r, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         r, r, r, r, o, o, o, _],
    ],
    [
        # Pressure normal
        # In this set of icons the balloon remaining intact conveys normal
        # pressure

        [_, _, _, _, _, _, _, _,
         g, g, g, _, _, _, _, _,
         g, _, g, _, _, _, _, _,
         g, _, g, _, g, g, g, _,
         g, g, g, _, _, _, _, _,
         g, _, _, _, g, g, g, _,
         g, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, _, g, g, g, g, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, g, g, g, g, _, _, _,
         g, g, g, g, g, g, _, _,
         g, g, g, g, g, g, _, _,
         g, g, g, g, g, g, _, _,
         _, g, g, g, g, _, _, _,
         _, _, _, o, _, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _],
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, r,
         _, g, g, g, g, g, r, _,
         _, g, g, g, g, r, g, _,
         _, _, g, g, r, g, _, _,
         _, r, _, r, o, _, _, _,
         _, _, r, _, _, o, _, _,
         _, _, _, _, _, _, o, _],

    ],
    [
        # Pressure high
        # In this set of icons the balloon bursts to show that pressure is too
        # high

        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, r, _, _,
         r, r, r, _, r, r, r, _,
         r, _, _, _, _, r, _, _,
         r, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, r, r, _, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, _, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, r, r, r, r, _, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, _, r, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [r, _, r, r, _, r, _, r,
         _, r, r, r, r, r, _, _,
         _, r, _, r, _, r, r, _,
         r, _, r, r, r, _, r, _,
         _, _, _, r, r, r, _, _,
         r, _, _, _, o, _, _, r,
         _, _, r, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
    ]
]

temperature_animations = [
    [
        # Low temperature
        # In this set of icons a snowman is used to portray low temperature

        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, r, r, r, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [b, b, w, w, w, w, b, b,
         b, w, w, w, w, w, w, b,
         w, w, _, w, w, _, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, o, o, w, w, w,
         w, w, _, w, w, _, w, w,
         b, w, w, _, _, w, w, b,
         b, b, w, w, w, w, b, b],
        [b, b, B, B, B, B, b, b,
         b, B, B, B, B, B, B, b,
         B, B, _, B, B, _, B, B,
         B, B, B, B, B, B, B, B,
         B, B, B, o, o, B, B, B,
         B, B, _, B, B, _, B, B,
         b, B, B, _, _, B, B, b,
         b, b, B, B, B, B, b, b],
        [b, b, b, b, b, b, b, b,
         b, w, b, b, w, b, b, w,
         b, b, w, b, w, b, w, b,
         b, b, b, w, w, w, b, b,
         b, w, w, b, w, b, w, w,
         b, b, b, w, w, w, b, b,
         b, b, w, b, w, b, w, b,
         b, w, b, b, w, b, b, w],
        [b, b, w, w, w, w, b, b,
         b, w, w, w, w, w, w, b,
         w, w, _, w, w, _, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, o, o, w, w, w,
         w, w, _, w, w, _, w, w,
         b, w, w, _, _, w, w, b,
         b, b, w, w, w, w, b, b],
        [b, b, B, B, B, B, b, b,
         b, B, B, B, B, B, B, b,
         B, B, _, B, B, _, B, B,
         B, B, B, B, B, B, B, B,
         B, B, B, o, o, B, B, B,
         B, B, _, B, B, _, B, B,
         b, B, B, _, _, B, B, b,
         b, b, B, B, B, B, b, b],
    ],
    [
         # Normal temperatue
         # In this set of icons we used a thermometer to portray normal
         # temperature

        [_, _, _, _, _, _, _, _,
         g, g, g, _, _, _, _, _,
         _, g, _, _, _, _, _, _,
         _, g, _, _, g, g, g, _,
         _, g, _, _, _, _, _, _,
         _, g, _, _, g, g, g, _,
         _, g, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, w, _, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, w, _, _, _, w, _, _,
         _, w, r, r, r, w, _, _,
         _, _, w, w, w, _, _, _],
        [_, _, _, w, _, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, w, r, r, r, w, _, _,
         _, w, r, r, r, w, _, _,
         _, _, w, w, w, _, _, _],
        [_, _, _, w, _, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, _, w, _, _, _,
         _, _, w, r, w, _, _, _,
         _, w, r, r, r, w, _, _,
         _, w, r, r, r, w, _, _,
         _, _, w, w, w, _, _, _],
        [_, _, _, w, _, _, _, _,
         _, _, w, _, w, _, _, g,
         _, _, w, _, w, _, g, _,
         _, _, w, _, w, g, _, _,
         _, _, w, r, g, _, _, _,
         _, g, r, g, r, w, _, _,
         _, w, g, r, r, w, _, _,
         _, _, w, w, w, _, _, _],
    ],
    [
        # High temperature
        # In set of icons there is a sun with animated rays which represents
        # too high temperature

        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, _, r, _, _,
         _, r, _, _, r, r, r, _,
         _, r, _, _, _, r, _, _,
         _, r, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, B, B, B, B, w, w,
         w, w, B, B, B, B, w, o,
         o, w, B, B, B, B, w, w,
         w, w, B, B, B, B, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [o, w, o, w, o, w, w, o,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, o,
         o, w, w, B, w, w, w, w,
         w, w, w, w, w, w, w, o,
         o, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, o,
         o, w, o, w, o, w, o, w],
        [o, w, o, w, o, w, w, o,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, o,
         o, w, w, B, B, w, w, w,
         w, w, w, B, B, w, w, o,
         o, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, o,
         o, w, o, w, o, w, o, w],
        [o, w, o, w, o, w, w, o,
         w, w, w, w, w, w, w, w,
         w, w, B, w, w, B, w, o,
         o, w, w, B, B, w, w, w,
         w, w, w, B, B, w, w, o,
         o, w, B, w, w, B, w, w,
         w, w, w, w, w, w, w, o,
         o, w, o, w, o, w, o, w],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, B, B, B, B, w, w,
         w, w, B, B, B, B, w, o,
         o, w, B, B, B, B, w, w,
         w, w, B, B, B, B, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
    ]
]

humidity_animations = [
    [
        # Humidity low
        # In this set of icons a raindrop transitioning from blue to white with
        # a black background represents low humidity

        [_, _, _, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, r, r, _, r, r, r, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, b, _, _, _,
         _, _, _, b, b, _, _, _,
         _, _, b, b, b, b, _, _,
         _, b, w, b, w, b, b, _,
         _, b, b, b, b, b, b, _,
         _, _, b, b, b, b, _, _,
         _, _, b, b, b, b, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, B, _, _, _,
         _, _, _, B, B, _, _, _,
         _, _, B, B, B, B, _, _,
         _, B, w, B, w, B, B, _,
         _, B, B, B, B, B, B, _,
         _, _, B, B, B, B, _, _,
         _, _, B, B, B, B, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, w, _, _, _,
         _, _, _, w, w, _, _, _,
         _, _, w, w, w, w, _, _,
         _, w, _, w, _, w, w, _,
         _, w, w, w, w, w, w, _,
         _, _, w, w, w, w, _, _,
         _, _, w, w, w, w, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, w, _, _, _,
         _, _, _, w, w, _, _, _,
         _, _, r, w, r, w, _, _,
         _, w, _, r, _, w, w, _,
         _, w, r, w, r, w, w, _,
         _, _, w, _, w, w, _, _,
         _, _, w, w, w, w, _, _,
         _, _, _, _, _, _, _, _],
    ],
    [
        # Normal humidity
        # In this set of icons a raindrop transitioning from white to blue with
        # a green background represents normal humidity

        [_, _, _, _, _, _, _, _,
         g, _, g, _, _, _, _, _,
         g, _, g, _, _, _, _, _,
         g, _, g, _, g, g, g, _,
         g, g, g, _, _, _, _, _,
         g, _, g, _, g, g, g, _,
         g, _, g, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [g, g, g, g, w, g, g, g,
         g, g, g, w, w, g, g, g,
         g, g, w, w, w, w, g, g,
         g, w, _, w, _, w, w, g,
         g, w, w, w, w, w, w, g,
         g, g, w, w, w, w, w, g,
         g, g, w, w, w, w, g, g,
         g, g, g, g, g, g, g, g],
        [g, g, g, g, B, g, g, g,
         g, g, g, B, B, g, g, g,
         g, g, B, B, B, B, g, g,
         g, B, _, B, _, B, B, g,
         g, B, B, B, B, B, B, g,
         g, g, B, B, B, B, g, g,
         g, g, B, B, B, B, g, g,
         g, g, g, g, g, g, g, g],
        [g, g, g, g, b, g, g, g,
         g, g, g, b, b, g, g, g,
         g, g, b, b, b, b, g, g,
         g, b, w, b, w, b, b, g,
         g, b, b, b, b, b, b, g,
         g, g, b, b, b, b, b, g,
         g, g, b, b, b, b, g, g,
         g, g, g, g, g, g, g, g],
        [g, g, g, g, b, g, g, g,
         g, g, g, b, b, g, g, r,
         g, g, b, b, b, b, r, g,
         g, b, w, b, w, r, b, g,
         g, b, b, b, r, b, b, g,
         g, r, b, r, b, b, b, g,
         g, g, r, b, b, b, g, g,
         g, g, g, g, g, g, g, g],
    ],
    [
        # High humidity
        # In this set of icons a raindrop transitioning from white to blue with
        # an orange background represents high humidity

        [_, _, _, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, r, _, _,
         r, r, r, _, r, r, r, _,
         r, _, r, _, _, r, _, _,
         r, _, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [o, o, o, o, w, o, o, o,
         o, o, o, w, w, o, o, o,
         o, o, w, w, w, w, o, o,
         o, w, _, w, _, w, w, o,
         o, w, w, w, w, w, w, o,
         o, o, w, _, w, w, o, o,
         o, o, w, w, w, w, o, o,
         o, o, o, o, o, o, o, o],
        [o, o, o, o, B, o, o, o,
         o, o, o, B, B, o, o, o,
         o, o, B, B, B, B, o, o,
         o, B, _, B, _, B, B, o,
         o, B, B, B, B, B, B, o,
         o, o, B, _, B, B, o, o,
         o, o, B, B, B, B, o, o,
         o, o, o, o, o, o, o, o],
        [o, o, o, o, b, o, o, o,
         o, o, o, b, b, o, o, o,
         o, o, b, b, b, b, o, o,
         o, b, _, b, _, b, b, o,
         o, b, b, b, b, b, b, o,
         o, o, b, _, b, b, o, o,
         o, o, b, b, b, b, o, o,
         o, o, o, o, o, o, o, o],
        [o, o, o, o, b, o, o, o,
         o, o, o, b, b, o, o, o,
         o, o, r, b, r, r, o, o,
         o, b, w, r, w, b, b, o,
         o, b, r, b, r, b, b, o,
         o, o, b, w, b, r, o, o,
         o, o, b, b, b, b, o, o,
         o, o, o, o, o, o, o, o],
    ]
]

light_animations = [
    [
        # Light low
        # In this set of icons there is a face that transitions out leaving only
        # a set of reflective eyes to show low light

        [_, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, r, r, r, _,
         r, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, p, p, p, p, _, _,
         _, w, w, p, w, w, p, _,
         _, w, e, p, w, e, p, _,
         _, p, p, p, p, p, p, _,
         _, p, p, _, _, p, p, _,
         _, _, p, p, p, p, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, w, w, _, w, w, _, _,
         _, w, e, _, w, e, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, w, w, _, w, w, _, _,
         _, e, w, _, e, w, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, w, w, _, w, w, _, _,
         _, w, e, _, w, e, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
    ],
    [
        # Normal light
        # In this set of icons a smiley face with animated eyes and a blue
        # background represents normal light

        [_, _, _, _, _, _, _, _,
         g, _, _, _, _, _, _, _,
         g, _, _, _, _, _, _, _,
         g, _, _, _, g, g, g, _,
         g, _, _, _, _, _, _, _,
         g, _, _, _, g, g, g, _,
         g, g, g, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [b, b, b, b, b, b, b, b,
         b, b, p, p, p, p, b, b,
         b, w, w, p, w, w, p, b,
         b, e, w, p, e, w, p, b,
         b, _, p, p, p, _, p, b,
         b, p, _, _, _, p, p, b,
         b, b, p, p, p, p, b, b,
         b, b, b, b, b, b, b, b],
        [b, b, b, b, b, b, b, b,
         b, b, p, p, p, p, b, b,
         b, w, w, p, w, w, p, b,
         b, w, e, p, w, e, p, b,
         b, _, p, p, p, _, p, b,
         b, p, _, _, _, p, p, b,
         b, b, p, p, p, p, b, b,
         b, b, b, b, b, b, b, b],
        [b, b, b, b, b, b, b, b,
         b, b, p, p, p, p, b, b,
         b, w, w, p, w, w, p, b,
         b, e, w, p, e, w, p, b,
         b, _, p, p, p, _, p, b,
         b, p, _, _, _, p, p, b,
         b, b, p, p, p, p, b, b,
         b, b, b, b, b, b, b, b],
        [b, b, b, b, b, b, b, b,
         b, b, p, p, p, p, b, b,
         b, w, w, p, w, w, p, b,
         b, w, e, p, w, e, p, b,
         b, _, p, p, p, _, p, b,
         b, p, _, _, _, p, p, b,
         b, b, p, p, p, p, b, b,
         b, b, b, b, b, b, b, b],
    ],
    [
        # Too much light
        # In this set of icons we have a sun with sunglasses
        # which represents too much light

        [_, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, r, _, _,
         r, _, _, _, r, r, r, _,
         r, _, _, _, _, r, _, _,
         r, r, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, _, y, _, y, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, _, y, _, y, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, _, y, _, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, _, y, _, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w],
    ]
]


# Get ready to talk to the Sense HAT

hat = SenseHat()

# Rotate the LED display to match orientation of Astro Pi case on
# ISS Columbus Module

hat.set_rotation(270)

# Get ready to talk to the Pi Camera module
#
# We aren't going to capture images or video, but we are
# going to use the module's ability to control gain and
# exposure to calculate luminosity in lux.
#
# Use sensor mode 5, which allows a wide framerate_range
# on either V1 or V2 camera modules, and set the widest
# range of possible frame rates that are supported by both
# camera modules.

cam = PiCamera(sensor_mode=5, framerate_range=(1, 40))

# Set up the camera so it is free to choose its exposure times and gains

cam.exposure_mode = 'auto'

# Use the metering mode that takes into account the largest area of the
# sensor, but without causing side effects for exposure and gain
# calculations (i.e., choose 'average' over 'backlit')

cam.meter_mode = 'average'

# Let the camera module automatically set its gain

cam.iso = 0


# SENSORS

class SensorReadings:

    def __init__(self, hat, cam, f):
        """
            Read all  sensors and compute all derived values
            when we create an object of this class.
        """

        self.f = f

        # Sense HAT

        self.temperature_from_humidity = hat.get_temperature_from_humidity()
        self.temperature_from_pressure = hat.get_temperature_from_pressure()
        self.humidity = hat.get_humidity()
        self.pressure = hat.get_pressure()

        # Pi Camera Module

        self.cam_revision = cam.revision
        self.analog_gain = cam.analog_gain
        self.digital_gain = cam.digital_gain
        self.exposure_speed = cam.exposure_speed

        # Raspberry Pi CPU

        self.cpu_temperature = self.__cpu_temperature()

        # time represents the moment when the next statement
        # is executed after reading all the sensors

        self.time = datetime.datetime.utcnow()

        # Compute all the values derived from the sensor readings

        self.average_temperature = self.__calc_average_temperature()
        self.calibrated_temperature = self.__calc_calibrated_temperature()
        self.calibrated_humidity = self.__calc_calibrated_humidity()
        self.dewpoint = self.__calc_dewpoint()
        self.light = self.__calc_light()

    def is_valid(self):
        """
            Skip this sensor read if the pressure or light sensor yielded 0.
            this would mean that the either pressure or light sensor value is
            not usable and so the entire sensor read should be ignored
        """
        return self.pressure != 0 and self.light != 0

    def __cpu_temperature(self):
        """
            Read the temperature of the Raspberry Pi CPU in Celsius
        """
        res = os.popen('vcgencmd measure_temp').readline()
        return float(res.replace("temp=","").replace("'C\n",""))

    def __calc_average_temperature(self):
        """
            Average the value from the two temperature sensors on the Sense HAT
        """
        return (self.temperature_from_humidity +
                self.temperature_from_pressure) / 2

    def __calc_calibrated_temperature(self):
        """
            Attempt to compensate for the differences between the temperature in
            the Raspberry Pi case and outside the case
        """
        return self.average_temperature - (self.cpu_temperature / 5)

    def __calc_calibrated_humidity(self):
        """
            Using the calibrated temperature, compute a calibrated relative
            humidity
        """
        dp = self.__humidity_to_dewpoint(self.temperature_from_humidity,
                                         self.humidity)
        ct = self.calibrated_temperature
        return self.__dewpoint_to_humidity(dp, ct)

    def __humidity_to_dewpoint(self, t, rh):
        """
            Convert relative humidity to dewpoint temperature using the Magnus
            formula. See:
            https://www.raspberrypi.org/learning/sensing-the-weather/lesson-7/worksheet/
            https://github.com/watou/lua-weathermetrics/blob/master/weathermetrics.lua
        """
        return ((rh / 100) ** 0.125) * (112 + 0.9 * t) + (0.1 * t) - 112

    def __dewpoint_to_humidity(self, dp, t):
        """
            Convert dewpoint temperature to relative humidity (reverse Magnus
            formula). See:
            https://github.com/watou/lua-weathermetrics/blob/master/weathermetrics.lua
        """
        return 100 * (((112 - (0.1 * t) + dp)/(112 + (0.9 * t))) ** 8)

    def __calc_dewpoint(self):
        return self.__humidity_to_dewpoint(self.temperature_from_humidity,
                                           self.humidity)

    def __calc_light(self):
        """
            Using gain and exposure sensor readings already taken, calculate the
            lux value
        """
        # compensate for old vs. new camera modules

        if self.cam_revision == 'ov5647':
            # This is a V1 camera
            aperture = 2.9
            iso_conversion = 1.0
        else:
            aperture = 2.0
            iso_conversion = 1.84

        # Use the analog and digital gains for overall gain

        gain = self.analog_gain * self.digital_gain

        # ISO is 100x the gain, first converted depending on camera model

        iso = (gain / iso_conversion) * 100

        # we need exposure time in seconds, not microseconds

        speed = self.exposure_speed / 1000000.0

        try:
            # Luminance in candela per square metre is:
            # 12.4 multiplied by the lens aperture squared
            # divided by
            # exposure time in seconds multiplied by the film speed in ISO units
            cdsqm = ((12.4 * (aperture ** 2))) / (speed * iso)

            # Lux is candela per square metre multiplied by Pi
            return cdsqm * pi

        except ZeroDivisionError:
            # there is a chance that either speed or iso were read as zero,
            # in which case we cannot use this reading we use a light reading
            # of 0 to indicate that it is invalid
            return 0

    # DATA LOGGING

    def __str__(self):
        """
            Return a CSV version of myself as a string
        """
        return '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' % (self.time.isoformat(), self.temperature_from_humidity,
             self.temperature_from_pressure, self.humidity, self.pressure,
             self.cam_revision, self.analog_gain, self.digital_gain,
             self.exposure_speed, self.cpu_temperature,
             self.average_temperature, self.calibrated_temperature,
             self.calibrated_humidity, self.dewpoint, self.light)

    @staticmethod
    def write_csv_header(f):
        f.write('time,temperature_from_humidity,temperature_from_pressure,humidity,pressure,cam_revision,analog_gain,digital_gain,exposure_speed,cpu_temperature,average_temperature,calibrated_temperature,calibrated_humidity,dewpoint,light')
        f.write('\n')

    def write_csv(self):
        """
            The string representation of this object is in CSV format, so write
            it and a newline to the previously given stream.
        """
        self.f.write(str(self))
        self.f.write("\n")

def read_sensors(hat, cam, f=sys.stdout):

    readings = SensorReadings(hat, cam, f)

    readings.write_csv()

    # Ignore invalid sensor readings

    if not readings.is_valid():
        return

    if readings.calibrated_temperature <= TEMP_LOW:
        animations['temperature'] = temperature_animations[LOW]
    elif readings.calibrated_temperature >= TEMP_HIGH:
        animations['temperature'] = temperature_animations[HIGH]
    else:
        animations['temperature'] = temperature_animations[NORMAL]

    if readings.dewpoint <= DEWPOINT_LOW:
        animations['humidity'] = humidity_animations[LOW]
    elif readings.dewpoint >= DEWPOINT_HIGH:
        animations['humidity'] = humidity_animations[HIGH]
    else:
        animations['humidity'] = humidity_animations[NORMAL]

    if readings.pressure <= PRESSURE_LOW:
        animations['pressure'] = pressure_animations[LOW]
    elif readings.pressure >= PRESSURE_HIGH:
        animations['pressure'] = pressure_animations[HIGH]
    else:
        animations['pressure'] = pressure_animations[NORMAL]

    if readings.light <= LIGHT_LOW:
        animations['light'] = light_animations[LOW]
    elif readings.light >= LIGHT_HIGH:
        animations['light'] = light_animations[HIGH]
    else:
        animations['light'] = light_animations[NORMAL]

# ANIMATED ICONS

# There are initially no animations set for the sensors because no readings
# have yet been taken

animations = { 'temperature': [], 'humidity': [], 'pressure': [], 'light': [] }

# Always yield the next animation to perform

def animation_generator():
    while True:
        for sensor_name, animation in animations.items():
            yield animation

animation = animation_generator()

def animate(animation):
    for frame in animation:
        hat.set_pixels(frame)
        sleep(FRAMETIME)
    sleep(LINGERTIME)

# READ SENSORS

def next_sensor_read_time():
    return time() + SENSOR_READ_INTERVAL

# Read the sensors in the future
# This initial delay allows the camera sufficient time to
# adjust its gain and exposure speed for meaningful results

sensor_read_time = next_sensor_read_time()

def sensor_read_time_expired():
    return time() > sensor_read_time

def advance_sensor_read_time():
    global sensor_read_time
    sensor_read_time = next_sensor_read_time()

# MAIN PROGRAMME

# Open a file for logging data
# Use a unique file name based on the current date and time
# and buffer each line (flush to disk after each line)

try:
    csv_file_name = datetime.datetime.now().strftime("teolai-%Y%m%d-%H%M%S.csv")
    f = open(csv_file_name, 'w', 1)
except IOError:
    f = sys.stdout

# Write the header line to the data file so we know the column meanings

SensorReadings.write_csv_header(f)

def loop_check(hat, cam, f):
    # Check to see if it's time to read the sensors
    if sensor_read_time_expired():
        # read the sensors again
        read_sensors(hat, cam, f)
        # set to check in the future
        advance_sensor_read_time()

    # Animate the next animation
    animate(next(animation))

try:
    while True:
        loop_check(hat, cam, f)

except KeyboardInterrupt:
    hat.clear()
    cam.close()
    sys.exit()
