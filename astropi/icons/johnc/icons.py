from time import sleep
from sense_hat import SenseHat

# colours

_ = (0, 0, 0)
r = (237, 0, 6)
o = (231, 140, 0)
g = (0, 192, 24)
w = (255, 255, 255)

# icons

pressure_icons = [
    [
        [_, _, r, r, r, r, _, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, _, r, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, r, r, _, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, _, r, r, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, r, r, _, _, _, _,
         _, r, r, r, r, _, _, _,
         r, _, _, _, r, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [r, r, r, r, r, r, r, r,
         w, w, w, r, r, r, r, r,
         w, r, w, r, r, r, r, r,
         w, r, w, r, r, r, r, r,
         w, w, w, r, w, w, w, r,
         w, r, r, r, r, r, r, r,
         w, r, r, r, r, r, r, r,
         r, r, r, r, r, r, r, r]
    ],
    [
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, _, g, g, g, g, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, _, g, g, g, g, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, _, g, g, g, g, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [g, g, g, g, g, g, g, g,
         w, w, w, g, g, g, g, g,
         w, g, w, g, g, g, g, g,
         w, g, w, g, w, w, w, g,
         w, w, w, g, g, g, g, g,
         w, g, g, g, w, w, w, g,
         w, g, g, g, g, g, g, g,
         g, g, g, g, g, g, g, g]
    ],
    [
        [_, _, _, r, r, _, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, _, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, r, r, r, r, _, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, _, r, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [r, _, r, r, _, r, _, r,
         _, r, r, r, r, r, _, _,
         _, r, _, r, _, r, r, _,
         r, _, r, r, r, _, r, _,
         _, _, _, r, r, r, _, _,
         r, _, _, _, o, _, _, r,
         _, _, r, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [r, r, r, r, r, r, r, r,
         w, w, w, r, r, r, r, r,
         w, r, w, r, r, r, r, r,
         w, r, w, r, r, w, r, r,
         w, w, w, r, w, w, w, r,
         w, r, r, r, r, w, r, r,
         w, r, r, r, r, r, r, r,
         r, r, r, r, r, r, r, r]
    ]
]

# Constant values

FRAMETIME = 0.25 # time between images is half a second
LOW = 0
NORMAL = 1
HIGH = 2

def animate_icons(icon_array):
    for icon in icon_array:
        hat.set_pixels(icon)
        sleep(FRAMETIME)
    sleep(1)

hat = SenseHat()

while True:
    animate_icons(pressure_icons[LOW])
    animate_icons(pressure_icons[NORMAL])
    animate_icons(pressure_icons[HIGH])
