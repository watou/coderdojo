from time import sleep
import datetime
import os
from math import pi
from sense_hat import SenseHat
from picamera import PiCamera
import random
import sys

# Constant values

FRAMETIME = 0.5 # time between images is half a second
SPINTIME = 0.10 # time an icon spins is one tenth of a second
SPINPROB = 0.20 # 20% probability that the image will spin
FRAMES = 10     # wait 10 frames before collecting sensor data

# Comfort Ranges

TEMP_LOW = 17.5
TEMP_HIGH = 30
DEWPOINT_LOW = 0
DEWPOINT_HIGH = 18
LIGHT_LOW = 500
LIGHT_HIGH = 1000000
PRESSURE_LOW = 1010
PRESSURE_HIGH = 1050

# Get ready to talk to the Sense HAT
hat = SenseHat()

# Get ready to talk to the Pi Camera module
#
# We aren't going to capture images or video, but we are
# going to use the module's ability to control gain and
# exposure to calculate luminosity in lux.
#
# Use sensor mode 5, which allows a wide framerate_range
# on either V1 or V2 camera modules, and set the widest
# range of possible frame rates that are supported by both
# camera modules.
cam = PiCamera(sensor_mode=5, framerate_range=(1, 40))

# Set up the camera so it is free to choose its exposure times and gains:
cam.exposure_mode = 'auto'

# Use the metering mode that takes into account the largest area of the
# sensor, but without causing side effects for exposure and gain
# calculations (i.e., choose 'average' over 'backlit')
cam.meter_mode = 'average'

# Let the camera module automatically set its gain
cam.iso = 0

# try to have more random results between runs
random.seed()

# indices for icons
LOW = 0
NORMAL = 1
HIGH = 2

# icons to show sensor conditions
temperature_icons = [
    [(0, 0, 0), (0, 0, 0), (127, 50, 1), (124, 51, 4), (122, 52, 12), (127, 50, 4), (0, 0, 0), (0, 0, 0), (0, 0, 0), (121, 52, 8), (124, 51, 4), (130, 86, 59), (6, 38, 236), (125, 87, 69), (121, 52, 8), (0, 0, 0), (0, 0, 0), (126, 51, 3), (250, 232, 126), (6, 17, 122), (251, 230, 129), (4, 20, 120), (121, 52, 8), (0, 0, 0), (0, 0, 0), (126, 51, 3), (252, 232, 124), (250, 232, 127), (250, 128, 120), (251, 230, 129), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (129, 86, 61), (83, 1, 128), (83, 1, 128), (129, 86, 61), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (83, 1, 128), (86, 0, 126), (86, 0, 126), (83, 1, 128), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (82, 3, 124), (0, 0, 0), (0, 0, 0), (82, 3, 124), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
    [(0, 0, 0), (0, 0, 0), (254, 0, 0), (254, 0, 0), (250, 1, 2), (245, 3, 5), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (254, 0, 0), (254, 0, 0), (246, 3, 5), (247, 249, 244), (240, 5, 10), (0, 0, 0), (0, 0, 0), (0, 0, 0), (250, 232, 126), (6, 17, 122), (249, 232, 128), (6, 17, 124), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (252, 232, 124), (250, 232, 127), (127, 50, 2), (126, 50, 8), (0, 0, 0), (0, 0, 0), (248, 230, 128), (241, 6, 3), (10, 16, 125), (239, 6, 13), (239, 6, 13), (10, 16, 125), (241, 6, 3), (248, 230, 128), (0, 0, 0), (0, 0, 0), (2, 20, 126), (10, 16, 125), (10, 16, 125), (2, 20, 126), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (3, 18, 124), (3, 19, 125), (3, 19, 125), (3, 18, 124), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (123, 50, 10), (0, 0, 0), (0, 0, 0), (123, 50, 10), (0, 0, 0), (0, 0, 0)], 
    [(0, 0, 0), (0, 0, 0), (192, 192, 192), (192, 192, 192), (192, 192, 192), (192, 192, 192), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (192, 192, 192), (192, 192, 192), (192, 192, 192), (192, 192, 192), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (251, 214, 13), (124, 2, 6), (253, 230, 124), (250, 216, 10), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (192, 191, 187), (193, 190, 190), (188, 192, 194), (192, 192, 188), (0, 0, 0), (0, 0, 0), (250, 232, 128), (5, 17, 124), (2, 18, 126), (190, 192, 193), (190, 192, 193), (2, 18, 126), (5, 17, 124), (250, 232, 128), (0, 0, 0), (0, 0, 0), (2, 18, 126), (158, 160, 161), (158, 160, 161), (2, 18, 126), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (48, 48, 48), (48, 48, 48), (48, 48, 48), (50, 50, 50), (0, 0, 0), (0, 0, 0), (2, 0, 0), (2, 0, 0), (206, 206, 206), (2, 0, 0), (2, 0, 0), (237, 237, 237), (2, 0, 0), (1, 0, 0)]
    ]

humidity_icons = [
    [(0, 0, 0), (0, 0, 0), (0, 107, 12), (0, 127, 14), (0, 127, 14), (0, 127, 14), (0, 20, 2), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 107, 12), (0, 127, 14), (0, 127, 14), (209, 232, 213), (47, 145, 60), (2, 19, 4), (0, 0, 0), (0, 0, 0), (211, 201, 124), (42, 52, 128), (210, 200, 129), (41, 51, 127), (1, 3, 19), (0, 0, 0), (0, 0, 0), (0, 0, 0), (213, 199, 123), (250, 233, 131), (145, 78, 23), (125, 50, 8), (19, 8, 1), (0, 0, 0), (209, 195, 111), (42, 142, 32), (2, 38, 105), (5, 108, 33), (8, 121, 23), (3, 34, 112), (11, 111, 29), (213, 216, 114), (0, 0, 0), (0, 0, 0), (2, 15, 107), (2, 20, 126), (2, 20, 123), (1, 19, 126), (0, 3, 19), (0, 0, 0), (0, 0, 0), (0, 0, 0), (2, 15, 107), (2, 19, 126), (3, 19, 124), (3, 18, 124), (0, 3, 19), (0, 0, 0), (0, 0, 0), (0, 0, 0), (106, 42, 8), (19, 8, 1), (0, 0, 0), (106, 42, 8), (19, 8, 1), (0, 0, 0)], 
    [(0, 0, 0), (0, 0, 0), (251, 216, 7), (127, 105, 6), (127, 105, 6), (251, 216, 7), (0, 0, 0), (0, 0, 0), (0, 0, 0), (248, 216, 13), (253, 215, 3), (251, 216, 7), (251, 216, 7), (253, 215, 3), (248, 216, 13), (0, 0, 0), (0, 0, 0), (251, 216, 8), (248, 214, 15), (7, 17, 125), (251, 230, 129), (4, 20, 120), (251, 216, 8), (0, 0, 0), (0, 0, 0), (251, 216, 8), (252, 232, 121), (250, 232, 127), (250, 128, 120), (251, 230, 129), (251, 216, 8), (0, 0, 0), (0, 0, 0), (250, 129, 227), (255, 127, 236), (255, 127, 236), (245, 128, 239), (253, 126, 234), (246, 128, 232), (0, 0, 0), (0, 0, 0), (252, 213, 17), (255, 127, 236), (255, 127, 236), (13, 145, 245), (245, 128, 239), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (252, 126, 235), (252, 126, 235), (252, 126, 235), (252, 126, 235), (0, 0, 0), (0, 0, 0), (0, 0, 0), (242, 8, 216), (250, 2, 220), (250, 2, 220), (250, 2, 220), (250, 2, 220), (242, 8, 216), (0, 0, 0)], 
    [(0, 0, 0), (0, 0, 0), (126, 51, 1), (126, 104, 3), (126, 104, 3), (126, 51, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0), (121, 52, 8), (124, 50, 3), (126, 51, 1), (126, 51, 1), (124, 50, 3), (121, 52, 8), (0, 0, 0), (0, 0, 0), (126, 51, 3), (124, 52, 7), (6, 19, 121), (251, 230, 129), (4, 20, 120), (126, 51, 3), (0, 0, 0), (0, 0, 0), (126, 51, 3), (252, 230, 128), (248, 232, 130), (250, 128, 120), (251, 230, 129), (126, 51, 3), (0, 0, 0), (0, 0, 0), (248, 216, 13), (255, 216, 0), (255, 216, 0), (247, 216, 8), (252, 216, 1), (248, 216, 13), (0, 0, 0), (0, 0, 0), (0, 0, 0), (255, 216, 0), (255, 216, 0), (10, 126, 64), (247, 216, 8), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (252, 214, 4), (252, 214, 4), (252, 214, 4), (252, 214, 4), (0, 0, 0), (0, 0, 0), (0, 0, 0), (245, 106, 10), (252, 105, 2), (252, 105, 2), (252, 105, 2), (252, 105, 2), (245, 106, 10), (0, 0, 0)]
    ]

light_icons = [
    [(0, 0, 0), (8, 124, 19), (0, 127, 14), (0, 127, 14), (5, 124, 16), (0, 0, 0), (0, 0, 0), (128, 128, 128), (0, 0, 0), (0, 0, 0), (0, 127, 14), (0, 127, 14), (1, 126, 14), (5, 124, 16), (0, 0, 0), (128, 128, 128), (0, 0, 0), (0, 0, 0), (250, 216, 10), (4, 1, 10), (250, 231, 127), (3, 2, 4), (0, 0, 0), (128, 128, 128), (0, 0, 0), (0, 0, 0), (253, 231, 126), (253, 232, 125), (250, 126, 123), (250, 231, 127), (0, 0, 0), (128, 128, 128), (248, 231, 129), (7, 126, 12), (0, 127, 14), (0, 127, 14), (0, 127, 14), (0, 127, 14), (6, 126, 16), (249, 230, 124), (0, 0, 0), (0, 0, 0), (0, 127, 14), (0, 127, 14), (0, 127, 14), (0, 127, 14), (0, 0, 0), (29, 2, 122), (0, 0, 0), (0, 0, 0), (126, 88, 62), (126, 88, 63), (126, 88, 63), (126, 88, 62), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (123, 52, 6), (0, 0, 0), (0, 0, 0), (123, 52, 6), (0, 0, 0), (0, 0, 0)], 
    [(0, 0, 0), (0, 0, 0), (250, 1, 2), (245, 3, 5), (245, 3, 5), (250, 1, 2), (0, 0, 0), (0, 0, 0), (0, 0, 0), (240, 5, 10), (246, 3, 5), (247, 249, 244), (247, 249, 244), (246, 3, 5), (240, 5, 10), (0, 0, 0), (0, 0, 0), (0, 0, 0), (241, 6, 3), (249, 250, 250), (250, 251, 246), (240, 6, 6), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (248, 230, 128), (2, 2, 2), (248, 233, 128), (8, 1, 5), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (249, 232, 128), (252, 230, 122), (249, 126, 128), (250, 232, 128), (0, 0, 0), (0, 0, 0), (0, 0, 0), (250, 233, 125), (6, 40, 233), (249, 232, 128), (250, 232, 128), (5, 38, 232), (250, 233, 125), (0, 0, 0), (0, 0, 0), (0, 0, 0), (5, 40, 237), (0, 0, 0), (0, 0, 0), (5, 40, 237), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (124, 50, 15), (0, 0, 0), (0, 0, 0), (124, 50, 15), (0, 0, 0), (0, 0, 0)],
    [(0, 0, 0), (0, 0, 0), (127, 50, 1), (124, 51, 4), (122, 52, 12), (127, 50, 4), (0, 0, 0), (0, 0, 0), (0, 0, 0), (121, 52, 8), (124, 51, 4), (130, 86, 59), (6, 38, 236), (125, 87, 69), (121, 52, 8), (0, 0, 0), (0, 0, 0), (126, 51, 3), (250, 232, 126), (6, 17, 122), (251, 230, 129), (4, 20, 120), (121, 52, 8), (0, 0, 0), (0, 0, 0), (126, 51, 3), (252, 232, 124), (250, 232, 127), (250, 128, 120), (251, 230, 129), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (129, 86, 61), (83, 1, 128), (83, 1, 128), (129, 86, 61), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (83, 1, 128), (86, 0, 126), (86, 0, 126), (83, 1, 128), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (82, 3, 124), (0, 0, 0), (0, 0, 0), (82, 3, 124), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)]
    ]

pressure_icons = [
    [(0, 0, 0), (0, 0, 0), (254, 0, 0), (254, 0, 0), (250, 1, 2), (245, 3, 5), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (254, 0, 0), (254, 0, 0), (246, 3, 5), (247, 249, 244), (240, 5, 10), (0, 0, 0), (0, 0, 0), (0, 0, 0), (250, 232, 126), (6, 17, 122), (249, 232, 128), (6, 17, 124), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (252, 232, 124), (250, 232, 127), (127, 50, 2), (126, 50, 8), (0, 0, 0), (0, 0, 0), (248, 230, 128), (241, 6, 3), (10, 16, 125), (239, 6, 13), (239, 6, 13), (10, 16, 125), (241, 6, 3), (248, 230, 128), (0, 0, 0), (0, 0, 0), (2, 20, 126), (10, 16, 125), (10, 16, 125), (2, 20, 126), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (3, 18, 124), (3, 19, 125), (3, 19, 125), (3, 18, 124), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (123, 50, 10), (0, 0, 0), (0, 0, 0), (123, 50, 10), (0, 0, 0), (0, 0, 0)], 
    [(0, 0, 0), (0, 0, 0), (192, 192, 192), (192, 192, 192), (192, 192, 192), (192, 192, 192), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (192, 192, 192), (192, 192, 192), (192, 192, 192), (192, 192, 192), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (251, 214, 13), (124, 2, 6), (253, 230, 124), (250, 216, 10), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (192, 191, 187), (193, 190, 190), (188, 192, 194), (192, 192, 188), (0, 0, 0), (0, 0, 0), (250, 232, 128), (5, 17, 124), (2, 18, 126), (190, 192, 193), (190, 192, 193), (2, 18, 126), (5, 17, 124), (250, 232, 128), (0, 0, 0), (0, 0, 0), (2, 18, 126), (158, 160, 161), (158, 160, 161), (2, 18, 126), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (48, 48, 48), (48, 48, 48), (48, 48, 48), (50, 50, 50), (0, 0, 0), (0, 0, 0), (2, 0, 0), (2, 0, 0), (206, 206, 206), (2, 0, 0), (2, 0, 0), (237, 237, 237), (2, 0, 0), (1, 0, 0)], 
    [(0, 0, 0), (0, 0, 0), (0, 107, 12), (0, 127, 14), (0, 127, 14), (0, 127, 14), (0, 20, 2), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 107, 12), (0, 127, 14), (0, 127, 14), (209, 232, 213), (47, 145, 60), (2, 19, 4), (0, 0, 0), (0, 0, 0), (211, 201, 124), (42, 52, 128), (210, 200, 129), (41, 51, 127), (1, 3, 19), (0, 0, 0), (0, 0, 0), (0, 0, 0), (213, 199, 123), (250, 233, 131), (145, 78, 23), (125, 50, 8), (19, 8, 1), (0, 0, 0), (209, 195, 111), (42, 142, 32), (2, 38, 105), (5, 108, 33), (8, 121, 23), (3, 34, 112), (11, 111, 29), (213, 216, 114), (0, 0, 0), (0, 0, 0), (2, 15, 107), (2, 20, 126), (2, 20, 123), (1, 19, 126), (0, 3, 19), (0, 0, 0), (0, 0, 0), (0, 0, 0), (2, 15, 107), (2, 19, 126), (3, 19, 124), (3, 18, 124), (0, 3, 19), (0, 0, 0), (0, 0, 0), (0, 0, 0), (106, 42, 8), (19, 8, 1), (0, 0, 0), (106, 42, 8), (19, 8, 1), (0, 0, 0)]
    ]

class SensorReadings:
    csv_header = "time,temperature,humidity,calibrated_temperature,dewpoint,pressure,light,temp_from_prs,cpu_temp"

    """ Read all the sensors when we create an object of this class """
    def __init__(self, hat, cam, f):
        self.temperature_from_humidity = hat.get_temperature_from_humidity()
        self.temperature_from_pressure = hat.get_temperature_from_pressure()
        self.humidity = hat.get_humidity()
        self.pressure = hat.get_pressure()
        self.cpu_temperature = self.__cpu_temperature()
        self.__calc_light(cam)
        self.f = f

    def __cpu_temperature(self):
        return 40 # so it works at trinket.io/sense-hat
        res = os.popen('vcgencmd measure_temp').readline()
        return float(res.replace("temp=","").replace("'C\n",""))

    def __calc_light(self, cam):
        # compensate for old vs. new camera modules
        if cam.revision == 'ov5647':
            # This is a V1 camera
            aperture = 2.9
            iso_conversion = 1.0
        else:
            aperture = 2.0
            iso_conversion = 1.84

        # Use the analog and digital gains for overall gain
        analog_gain = cam.analog_gain
        digital_gain = cam.digital_gain
        gain = analog_gain * digital_gain
        # print(gain, '=', analog_gain, '*', digital_gain)

        # ISO is 100x the gain, first converted depending on camera model
        iso = (gain / iso_conversion) * 100

        # we need exposure time in seconds, not microseconds
        speed = cam.exposure_speed / 1000000.0
        # print(speed)

        # Luminance in candela per square metre is:
        # 12.4 multiplied by the lens aperture squared
        # divided by
        # exposure time in seconds multiplied by the film speed in ISO units
        cdsqm = ((12.4 * (aperture ** 2))) / (speed * iso)

        # Lux is candela per square metre multiplied by Pi
        self.light = cdsqm * pi

    def is_valid(self):
        """ skip this sensor read if the pressure sensor returned zero. this would mean that the pressure sensor value is not usable and so the enter sensor read should be ignored """
        return self.pressure != 0

    def get_average_temperature(self):
        """ Average the value from the two temperature sensors on the Sense HAT """
        return (self.temperature_from_humidity + self.temperature_from_pressure) / 2

    def get_calibrated_temperature(self):
        """ Attempt to compensate for the differences between the temperature in the Raspberry Pi case and outside the case """
        # return 0.0071*self.temperature_from_humidity*self.temperature_from_humidity+0.86*self.temperature_from_humidity-10.0
        # return self.temperature_from_humidity - 14 - ((self.cpu_temperature - self.temperature_from_humidity) / 25)
        return self.get_average_temperature() - (self.cpu_temperature / 5)

    def get_calibrated_humidity(self):
        """ Using the calibrated temperature, compute a calibrated relative humidity """
        # return self.humidity*(2.5*0.029*self.temperature_from_humidity)
        dp = self.__humidity_to_dewpoint(self.temperature_from_humidity, self.humidity)
        ct = self.get_calibrated_temperature()
        return self.__dewpoint_to_humidity(dp, ct)

    # convert relative humidity to dewpoint temperature using the Magnus formula
    # see https://www.raspberrypi.org/learning/sensing-the-weather/lesson-7/worksheet/
    # see https://github.com/watou/lua-weathermetrics/blob/master/weathermetrics.lua
    def __humidity_to_dewpoint(self, t, rh):
        return ((rh / 100) ** 0.125) * (112 + 0.9 * t) + (0.1 * t) - 112

    # convert depoint temperature to relative humidity (reverse Magnus formula)
    # see https://github.com/watou/lua-weathermetrics/blob/master/weathermetrics.lua
    def __dewpoint_to_humidity(self, dp, t):
        return 100 * (((112 - (0.1 * t) + dp)/(112 + (0.9 * t))) ** 8)

    def get_dewpoint(self):
        return self.__humidity_to_dewpoint(self.temperature_from_pressure, self.humidity)

    def get_pressure(self):
        return self.pressure

    def get_light(self):
        return self.light

    def print_csv(self):
        self.f.write(str(datetime.datetime.utcnow().isoformat()))
        self.f.write(",")
        self.f.write(str(self.temperature_from_humidity))
        self.f.write(",")
        self.f.write(str(self.humidity))
        self.f.write(",")
        self.f.write(str(self.get_calibrated_temperature()))
        self.f.write(",")
        self.f.write(str(self.get_dewpoint()))
        self.f.write(",")
        self.f.write(str(self.get_pressure()))
        self.f.write(",")
        self.f.write(str(self.get_light()))
        self.f.write(",")
        self.f.write(str(self.temperature_from_pressure))
        self.f.write(",")
        self.f.write(str(self.cpu_temperature))
        self.f.write("\n")

def read_sensors(hat, cam):

    readings = SensorReadings(hat, cam, sys.stdout)
    if not readings.is_valid():
        return

    readings.print_csv()

    calibrated_temperature = readings.get_calibrated_temperature()
    if calibrated_temperature < TEMP_LOW:
        temperature_icon = temperature_icons[LOW]
    elif calibrated_temperature > TEMP_HIGH:
        temperature_icon = temperature_icons[HIGH]
    else:
        temperature_icon = temperature_icons[NORMAL]

    dewpoint = readings.get_dewpoint()
    if dewpoint < DEWPOINT_LOW:
        humidity_icon = humidity_icons[LOW]
    elif dewpoint > DEWPOINT_HIGH:
        humidity_icon = humidity_icons[HIGH]
    else:
        humidity_icon = humidity_icons[NORMAL]

    pressure = readings.get_pressure()
    if pressure < PRESSURE_LOW:
        pressure_icon = pressure_icons[LOW]
    elif pressure > PRESSURE_HIGH:
        pressure_icon = pressure_icons[HIGH]
    else:
        pressure_icon = pressure_icons[NORMAL]

    light = readings.get_light()
    if light < LIGHT_LOW:
        light_icon = light_icons[LOW]
    elif light > LIGHT_HIGH:
        light_icon = light_icons[HIGH]
    else:
        light_icon = light_icons[NORMAL]

def show_icon(icon):
    hat.set_pixels(icon)
    # if a random number between 0 and 1 is less than SPINPROB...
    if random.random() <= SPINPROB:
        # spin the image
        sleep(SPINTIME)
        hat.set_rotation(90)
        sleep(SPINTIME)
        hat.set_rotation(180)
        sleep(SPINTIME)
        hat.set_rotation(270)
        sleep(SPINTIME)
        hat.set_rotation(0)
    # wait before doing anything more
    sleep(FRAMETIME)

# initial settings for the loop

frame = 0

temperature_icon = temperature_icons[NORMAL]
humidity_icon = humidity_icons[NORMAL]
pressure_icon = pressure_icons[NORMAL]
light_icon = light_icons[NORMAL]

print(SensorReadings.csv_header)

# main loop

try:
    while True:

        # see if FRAMES number of frames has passed
        if frame % FRAMES == 0:
            read_sensors(hat, cam)
        frame += 1  # same as typing: frame = frame + 1

        show_icon(temperature_icon)
        show_icon(humidity_icon)
        show_icon(pressure_icon)
        show_icon(light_icon)

# just a way to black out the LED matrix when we're done, with ctrl-C
except KeyboardInterrupt:
    hat.clear()
    cam.close()
    sys.exit()
