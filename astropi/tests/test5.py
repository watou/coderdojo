from time import sleep, time
import datetime
import os
from math import pi
from sense_hat import SenseHat
from picamera import PiCamera
import sys

# CONSTANT VALUES

SENSOR_READ_INTERVAL = 5 # read sensors no more often than 5 seconds
FRAMETIME = 0.3 # time between frames is three tenths of a second
LINGERTIME = 1 # linger on an important frame for one second

# COMFORT RANGES

TEMP_LOW = 17.5
TEMP_HIGH = 30
DEWPOINT_LOW = 0
DEWPOINT_HIGH = 18
LIGHT_LOW = 500
LIGHT_HIGH = 1000000
PRESSURE_LOW = 1010
PRESSURE_HIGH = 1050

# COLOURS

_ = (0, 0, 0)
r = (237, 0, 6)
o = (231, 140, 0)
g = (0, 192, 24)
w = (255, 255, 255)
y = (255, 255, 0)

b1 = (67,110,238) #KYBLUE1
w1 = (255, 255, 255) #WHITE
y1 = (255,255,0) #YELLOW
r1 = (255,0,0) #RED
g1 = (0,100,0) #DARKGREEN
o1 = (255,165,0) #ORANGE

p = (255,160,122) # peach
e = (127,127,127) # grey

# ANIMATIONS

LOW = 0
NORMAL = 1
HIGH = 2

pressure_animations = [
    [
        [_, _, r, r, r, r, _, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, _, r, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, r, r, _, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, _, r, r, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, r, r, _, _, _, _,
         _, r, r, r, r, _, _, _,
         r, _, _, _, r, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         r, r, r, r, o, o, o, _],
        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, r, r, _, r, r, r, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, _, g, g, g, g, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, g, g, g, g, _, _, _,
         g, g, g, g, g, g, _, _,
         g, g, g, g, g, g, _, _,
         g, g, g, g, g, g, _, _,
         _, g, g, g, g, _, _, _,
         _, _, _, o, _, _, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _],
        [_, _, g, g, g, g, _, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, g, g, g, g, g, g, _,
         _, _, g, g, g, g, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         g, g, g, _, _, _, _, _,
         g, _, g, _, _, _, _, _,
         g, _, g, _, g, g, g, _,
         g, g, g, _, _, _, _, _,
         g, _, _, _, g, g, g, _,
         g, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [_, _, _, r, r, _, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, r, r, r, r, _, _,
         _, _, _, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, r, r, r, r, _, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, r, r, r, r, r, r, _,
         _, _, r, r, r, r, _, _,
         _, _, _, _, o, _, _, _,
         _, _, _, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [r, _, r, r, _, r, _, r,
         _, r, r, r, r, r, _, _,
         _, r, _, r, _, r, r, _,
         r, _, r, r, r, _, r, _,
         _, _, _, r, r, r, _, _,
         r, _, _, _, o, _, _, r,
         _, _, r, _, _, o, _, _,
         _, _, _, _, _, _, o, _],
        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, r, _, _,
         r, r, r, _, r, r, r, _,
         r, _, _, _, _, r, _, _,
         r, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ]
]

temperature_animations = [
    [
        [(4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), 
         (4, 135, 248), (249, 242, 242), (249, 242, 242), (0, 0, 0), (0, 0, 0), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248)], 
        [(4, 135, 248), (4, 135, 248), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (4, 135, 248), 
         (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), 
         (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), 
         (206, 232, 230), (206, 232, 230), (206, 232, 230), (253, 105, 7), (253, 105, 7), (206, 232, 230), (206, 232, 230), (206, 232, 230), 
         (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), 
         (4, 135, 248), (206, 232, 230), (206, 232, 230), (0, 0, 0), (0, 0, 0), (206, 232, 230), (206, 232, 230), (4, 135, 248), 
         (4, 135, 248), (4, 135, 248), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (4, 135, 248), (4, 135, 248)], 
        [(4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), 
         (4, 135, 248), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), 
         (4, 135, 248), (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), (249, 242, 242), 
         (4, 135, 248), (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248),
         (4, 135, 248), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242)], 
        [(4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), (0, 0, 0), (249, 242, 242), (249, 242, 242), 
         (4, 135, 248), (249, 242, 242), (249, 242, 242), (0, 0, 0), (0, 0, 0), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248)],
        [(4, 135, 248), (4, 135, 248), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (4, 135, 248), 
         (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), 
         (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), 
         (206, 232, 230), (206, 232, 230), (206, 232, 230), (253, 105, 7), (253, 105, 7), (206, 232, 230), (206, 232, 230), (206, 232, 230), 
         (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), (0, 0, 0), (206, 232, 230), (206, 232, 230), 
         (4, 135, 248), (206, 232, 230), (206, 232, 230), (0, 0, 0), (0, 0, 0), (206, 232, 230), (206, 232, 230), (4, 135, 248), 
         (4, 135, 248), (4, 135, 248), (206, 232, 230), (206, 232, 230), (206, 232, 230), (206, 232, 230), (4, 135, 248), (4, 135, 248)],
        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, r, r, r, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [(249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (4, 135, 248), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242),
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), 
         (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (4, 135, 248), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242)],
        [(249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (4, 135, 248), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242), 
         (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (4, 135, 248), (249, 242, 242), (249, 242, 242), (4, 135, 248), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242)],
        [(4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (253, 105, 7), (4, 135, 248), (4, 135, 248), (253, 105, 7),
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (4, 135, 248), (253, 105, 7), (4, 135, 248), 
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (253, 105, 7), (4, 135, 248), (4, 135, 248),
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (253, 105, 7), (4, 135, 248), (4, 135, 248),
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (4, 135, 248), (253, 105, 7), (4, 135, 248),
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (253, 105, 7), (4, 135, 248), (4, 135, 248), (253, 105, 7), 
         (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248)],
        [(4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (253, 105, 7), (4, 135, 248), (4, 135, 248), (253, 105, 7), 
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (4, 135, 248), (253, 105, 7), (4, 135, 248), 
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (253, 105, 7), (4, 135, 248), (4, 135, 248), 
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (253, 105, 7), (4, 135, 248), (4, 135, 248), 
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (4, 135, 248), (253, 105, 7), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (253, 105, 7), (4, 135, 248), (4, 135, 248), (253, 105, 7), 
         (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248)],
        [(4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (249, 242, 242), (4, 135, 248), 
         (249, 242, 242), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (249, 242, 242), 
         (249, 242, 242), (4, 135, 248), (249, 242, 242), (4, 135, 248), (4, 135, 248), (249, 242, 242), (4, 135, 248), (249, 242, 242),
         (249, 242, 242), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (4, 135, 248), (249, 242, 242), 
         (249, 242, 242), (4, 135, 248), (253, 105, 7), (4, 135, 248), (4, 135, 248), (253, 105, 7), (4, 135, 248), (249, 242, 242), 
         (4, 135, 248), (249, 242, 242), (4, 135, 248), (253, 105, 7), (253, 105, 7), (4, 135, 248), (249, 242, 242), (4, 135, 248), 
         (4, 135, 248), (4, 135, 248), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (4, 135, 248), (4, 135, 248)],
        [_, _, _, _, _, _, _, _,
         g, g, g, _, _, _, _, _,
         _, g, _, _, _, _, _, _,
         _, g, _, _, g, g, g, _,
         _, g, _, _, _, _, _, _,
         _, g, _, _, g, g, g, _,
         _, g, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [(249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (253, 105, 7), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7)],
        [(253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (255, 255, 0), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242)],
        [(253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242)],
        [(253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (255, 255, 0), (249, 242, 242), (249, 242, 242), (255, 255, 0), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (255, 255, 0), (249, 242, 242), (249, 242, 242), (255, 255, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242)],
        [(249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (253, 105, 7), (249, 242, 242), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (249, 242, 242), (255, 255, 0), (255, 255, 0), (255, 255, 0), (255, 255, 0), (249, 242, 242), (253, 105, 7), 
         (253, 105, 7), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), (249, 242, 242), 
         (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7), (249, 242, 242), (253, 105, 7)],
        [_, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         _, r, _, _, _, _, _, _,
         _, r, _, _, _, r, _, _,
         _, r, _, _, r, r, r, _,
         _, r, _, _, _, r, _, _,
         _, r, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ]
]

humidity_animations = [
    [
        [y1, y1, y1, y1, y1, y1, y1, y1,
         y1, y1, y1, g1, g1, y1, y1, y1,
         y1, g1, y1, g1, g1, y1, y1, y1, 
         y1, g1, y1, g1, g1, y1, g1, y1,
         y1, g1, g1, g1, g1, g1, g1, y1,
         y1, y1, y1, g1, g1, y1, y1, y1,
         y1, y1, y1, g1, g1, y1, y1, y1,
         y1, y1, y1, y1, y1, y1, y1, y1],
        [y1, y1, y1, y1, y1, y1, y1, y1,
         y1, y1, g1, g1, g1, y1, y1, y1, 
         y1, y1, y1, y1, g1, y1, y1, y1, 
         y1, g1, g1, g1, g1, g1, g1, y1,
         y1, g1, g1, g1, g1, g1, g1, y1,
         y1, y1, y1, y1, g1, y1, y1, y1,
         y1, y1, y1, g1, g1, y1, y1, y1, 
         y1, y1, y1, y1, y1, y1, y1, y1],
        [o1, o1, o1, o1, o1, o1, o1, o1, 
         o1, o1, o1, g1, g1,o1, o1, o1,
         o1, g1, o1, g1, g1, o1, o1, o1, 
         o1, g1, o1, g1, g1, o1, g1, o1,
         o1, g1, g1, g1, g1, g1, g1, o1,
         o1, o1, o1, g1, g1, o1, o1, o1,
         o1, o1, o1, g1, g1, o1, o1, o1,
         o1, o1, o1, o1, o1, o1, o1, o1],
        [y1, y1, y1, y1, y1, y1, y1, y1,
         y1, r1, y1, g1, g1, y1, r1, y1,
         y1, g1, r1, g1, g1, r1, y1, y1, 
         y1, g1, y1, r1, r1, y1, g1, y1,
         y1, g1, g1, r1, r1, g1, g1, y1,
         y1, y1, r1, g1, g1, r1, y1, y1,
         y1, r1, y1, g1, g1, y1, r1, y1,
         y1, y1, y1, y1, y1, y1, y1, y1],
        [_, _, _, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, r, r, _, r, r, r, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [b1, b1, b1, b1, b1, b1, b1, b1,
         b1, b1, b1, b1, w1, b1, b1, b1,
         b1, b1, b1, w1, w1, w1, b1, b1,
         b1, b1, w1, w1, w1, w1, w1, b1, 
         b1, b1, w1, w1, w1, w1, w1, b1, 
         b1, b1, w1, w1, w1, w1, b1, b1, 
         b1, b1, b1, w1, w1, b1, b1, b1,
         b1, b1, b1, b1, b1, b1, b1, b1],
        [w1, w1, w1, w1, w1, w1, w1, w1,
         w1, w1, w1, w1, b1, w1, w1, w1,
         w1, w1, w1, b1, b1, b1, w1, w1,
         w1, w1, b1, b1, b1, b1, b1, w1,
         w1, w1, b1, b1, b1, b1, b1, w1,
         w1, w1, b1, b1, b1, b1, w1, w1,
         w1, w1, w1, b1, b1, w1, w1, w1,
         w1, w1, w1, w1, w1, w1, w1, w1],
        [b1, b1, b1, b1, b1, b1, b1, b1,
         b1, b1, b1, b1, y1, b1, b1, b1,
         b1, b1, b1, y1, y1, y1, b1, b1,
         b1, b1, y1, y1, y1, y1, y1, b1, 
         b1, b1, y1, y1, y1, y1, y1, b1, 
         b1, b1, y1, y1, y1, y1, b1, b1, 
         b1, b1, b1, y1, y1, b1, b1, b1,
         b1, b1, b1, b1, b1, b1, b1, b1],
        [b1, b1, b1, b1, b1, b1, b1, b1,
         b1, b1, b1, b1, w1, b1, b1, r1,
         b1, b1, b1, w1, w1, w1, r1, b1,
         b1, b1, w1, w1, w1, r1, w1, b1, 
         b1, b1, w1, w1, r1, w1, w1, b1, 
         b1, r1, w1, r1, w1, w1, b1, b1, 
         b1, b1, r1, w1, w1, b1, b1, b1,
         b1, b1, b1, b1, b1, b1, b1, b1],
        [_, _, _, _, _, _, _, _,
         g, _, g, _, _, _, _, _,
         g, _, g, _, _, _, _, _,
         g, _, g, _, g, g, g, _,
         g, g, g, _, _, _, _, _,
         g, _, g, _, g, g, g, _,
         g, _, g, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [w1, w1, w1, w1, w1, w1, w1, w1,
         w1, b1, w1, w1, w1, b1, w1, w1, 
         w1, b1, w1, b1, w1, b1, w1, w1, 
         w1, w1, w1, b1, w1, w1, w1, w1,
         w1, b1, w1, w1, w1, w1, b1, w1,
         w1, b1, w1, w1, b1, w1, w1, w1, 
         w1, w1, w1, w1, b1, w1, w1, w1, 
         w1, w1, b1, w1, w1, w1, w1, w1],
        [b1, b1, b1, b1, b1, b1, b1, b1,
         b1, w1, r1, r1, r1, r1, r1, b1, 
         b1, r1, b1, b1, r1, b1, b1, r1, 
         b1, b1, b1, b1, r1, b1, b1, b1,
         b1, w1, b1, b1, r1, b1, w1, b1,
         b1, w1, r1, b1, r1, b1, b1, b1, 
         b1, b1, b1, r1, w1, b1, b1, b1, 
         b1, b1, w1, b1, b1, b1, b1, b1],
        [w1, w1, w1, w1, w1, w1, w1, w1,
         w1, b1, w1, w1, w1, b1, w1, w1, 
         w1, b1, w1, b1, w1, b1, w1, w1, 
         w1, w1, w1, b1, w1, w1, w1, w1,
         w1, b1, w1, w1, w1, w1, b1, w1,
         w1, b1, w1, w1, b1, w1, w1, w1, 
         w1, w1, w1, w1, b1, w1, w1, w1, 
         w1, w1, b1, w1, w1, w1, w1, w1],
        [w1, w1, w1, w1, w1, w1, w1, w1,
         w1, r1, w1, w1, w1, b1, r1, w1, 
         w1, b1, r1, b1, w1, r1, w1, w1, 
         w1, w1, w1, r1, r1, w1, w1, w1,
         w1, b1, w1, r1, r1, w1, b1, w1,
         w1, b1, r1, w1, b1, r1, w1, w1, 
         w1, r1, w1, w1, b1, w1, r1, w1, 
         w1, w1, b1, w1, w1, w1, w1, w1],
        [_, _, _, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, _, _, _,
         r, _, r, _, _, r, _, _,
         r, r, r, _, r, r, r, _,
         r, _, r, _, _, r, _, _,
         r, _, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _]    
    ]
]

light_animations = [
    [
        [_, _, _, _, _, _, _, _,
         _, _, p, p, p, p, _, _,
         _, w, w, p, w, w, p, _,
         _, w, e, p, w, e, p, _,
         _, p, p, p, p, p, p, _,
         _, p, p, _, _, p, p, _,
         _, _, p, p, p, p, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, w, w, _, w, w, _, _,
         _, w, e, _, w, e, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, w, w, _, w, w, _, _,
         _, e, w, _, e, w, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, w, w, _, w, w, _, _,
         _, w, e, _, w, e, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _,
         _, _, _, _, _, _, _, _],
        [_, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, r, r, r, _,
         r, _, _, _, _, _, _, _,
         r, r, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [_, _, _, _, _, _, _, _,
         g, _, _, _, _, _, _, _,
         g, _, _, _, _, _, _, _,
         g, _, _, _, g, g, g, _,
         g, _, _, _, _, _, _, _,
         g, _, _, _, g, g, g, _,
         g, g, g, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ],
    [
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, _, y, _, y, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, _, y, _, y, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, _, y, _, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, _, y, _, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w],
        [w, o, w, o, w, o, o, w,
         o, w, w, w, w, w, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, _, y, _, w, o,
         o, w, y, y, y, y, w, w,
         w, w, y, y, y, y, w, o,
         o, w, w, w, w, w, w, w,
         w, o, w, o, w, o, w, o],
        [w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w,
         w, w, w, w, w, w, w, w],
        [_, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, _, _, _,
         r, _, _, _, _, r, _, _,
         r, _, _, _, r, r, r, _,
         r, _, _, _, _, r, _, _,
         r, r, r, _, _, _, _, _,
         _, _, _, _, _, _, _, _]
    ]
]

# Get ready to talk to the Sense HAT

hat = SenseHat()

# Rotate the LED display to match orientation of Astro Pi case on ISS Columbus Module

hat.set_rotation(270)

# Get ready to talk to the Pi Camera module
#
# We aren't going to capture images or video, but we are
# going to use the module's ability to control gain and
# exposure to calculate luminosity in lux.
#
# Use sensor mode 5, which allows a wide framerate_range
# on either V1 or V2 camera modules, and set the widest
# range of possible frame rates that are supported by both
# camera modules.

cam = PiCamera(sensor_mode=5, framerate_range=(1, 40))

# Set up the camera so it is free to choose its exposure times and gains:

cam.exposure_mode = 'auto'

# Use the metering mode that takes into account the largest area of the
# sensor, but without causing side effects for exposure and gain
# calculations (i.e., choose 'average' over 'backlit')

cam.meter_mode = 'average'

# Let the camera module automatically set its gain

cam.iso = 0


# SENSORS

class SensorReadings:

    def __init__(self, hat, cam, f):
        """ Read all  sensors and compute all derived values
            when we create an object of this class.
        """

        self.f = f

        # Sense HAT

        self.temperature_from_humidity = hat.get_temperature_from_humidity()
        self.temperature_from_pressure = hat.get_temperature_from_pressure()
        self.humidity = hat.get_humidity()
        self.pressure = hat.get_pressure()

        # Pi Camera Module

        self.cam_revision = cam.revision
        self.analog_gain = cam.analog_gain
        self.digital_gain = cam.digital_gain
        self.exposure_speed = cam.exposure_speed

        # Raspberry Pi CPU

        self.cpu_temperature = self.__cpu_temperature()

        # time represents the moment when the next statement 
        # is executed after reading all the sensors

        self.time = datetime.datetime.utcnow()

        # Compute all the values derived from the sensor readings

        self.average_temperature = self.__calc_average_temperature()
        self.calibrated_temperature = self.__calc_calibrated_temperature()
        self.calibrated_humidity = self.__calc_calibrated_humidity()
        self.dewpoint = self.__calc_dewpoint()
        self.light = self.__calc_light()

    def is_valid(self):
        """ skip this sensor read if the pressure or light sensor yielded 0.
            this would mean that the either pressure or light sensor value is not
            usable and so the entire sensor read should be ignored
        """
        return self.pressure != 0 and self.light != 0

    def __cpu_temperature(self):
        """ Read the temperature of the Raspberry Pi CPU in Celsius
        """
        res = os.popen('vcgencmd measure_temp').readline()
        return float(res.replace("temp=","").replace("'C\n",""))

    def __calc_average_temperature(self):
        """ Average the value from the two temperature sensors on the Sense HAT 
        """
        return (self.temperature_from_humidity + self.temperature_from_pressure) / 2

    def __calc_calibrated_temperature(self):
        """ Attempt to compensate for the differences between the temperature in the Raspberry Pi case and outside the case 
        """
        return self.average_temperature - (self.cpu_temperature / 5)

    def __calc_calibrated_humidity(self):
        """ Using the calibrated temperature, compute a calibrated relative humidity """
        # return self.humidity*(2.5*0.029*self.temperature_from_humidity)
        dp = self.__humidity_to_dewpoint(self.temperature_from_humidity, self.humidity)
        ct = self.calibrated_temperature
        return self.__dewpoint_to_humidity(dp, ct)

    # convert relative humidity to dewpoint temperature using the Magnus formula
    # see https://www.raspberrypi.org/learning/sensing-the-weather/lesson-7/worksheet/
    # see https://github.com/watou/lua-weathermetrics/blob/master/weathermetrics.lua
    def __humidity_to_dewpoint(self, t, rh):
        return ((rh / 100) ** 0.125) * (112 + 0.9 * t) + (0.1 * t) - 112

    # convert dewpoint temperature to relative humidity (reverse Magnus formula)
    # see https://github.com/watou/lua-weathermetrics/blob/master/weathermetrics.lua
    def __dewpoint_to_humidity(self, dp, t):
        return 100 * (((112 - (0.1 * t) + dp)/(112 + (0.9 * t))) ** 8)

    def __calc_dewpoint(self):
        return self.__humidity_to_dewpoint(self.temperature_from_humidity, self.humidity)

    def __calc_light(self):
        """ Using gain and exposure sensor readings already taken, calculate the lux value
        """

        # compensate for old vs. new camera modules

        if self.cam_revision == 'ov5647':
            # This is a V1 camera
            aperture = 2.9
            iso_conversion = 1.0
        else:
            aperture = 2.0
            iso_conversion = 1.84

        # Use the analog and digital gains for overall gain

        gain = self.analog_gain * self.digital_gain

        # ISO is 100x the gain, first converted depending on camera model

        iso = (gain / iso_conversion) * 100

        # we need exposure time in seconds, not microseconds

        speed = self.exposure_speed / 1000000.0

        try:
            # Luminance in candela per square metre is:
            # 12.4 multiplied by the lens aperture squared
            # divided by
            # exposure time in seconds multiplied by the film speed in ISO units
            cdsqm = ((12.4 * (aperture ** 2))) / (speed * iso)

            # Lux is candela per square metre multiplied by Pi
            return cdsqm * pi

        except ZeroDivisionError:
            # there is a chance that either speed or iso were read as zero, in which case we cannot use this reading
            # we use a light reading of 0 to indicate that it is invalid
            return 0

    # DATA LOGGING

    def __str__(self):
        """ Return a CSV version of myself as a string 
        """
        return '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' % (self.time.isoformat(),
            self.temperature_from_humidity, self.temperature_from_pressure, self.humidity, self.pressure, 
            self.cam_revision, self.analog_gain, self.digital_gain, self.exposure_speed, self.cpu_temperature,
            self.average_temperature, self.calibrated_temperature, self.calibrated_humidity, self.dewpoint, self.light)

    @staticmethod
    def write_csv_header(f):
        f.write('time,temperature_from_humidity,temperature_from_pressure,humidity,pressure,cam_revision,analog_gain,digital_gain,exposure_speed,cpu_temperature,average_temperature,calibrated_temperature,calibrated_humidity,dewpoint,light')
        f.write('\n')

    def write_csv(self):
        """ The string representation of this object is in CSV format, so write
            it and a newline to the previously given stream.
        """
        self.f.write(str(self))
        self.f.write("\n")

def read_sensors(hat, cam, f=sys.stdout):

    readings = SensorReadings(hat, cam, f)

    readings.write_csv()

    # Ignore invalid sensor readings

    if not readings.is_valid():
        return

    if readings.calibrated_temperature <= TEMP_LOW:
        animations['temperature'] = temperature_animations[LOW]
    elif readings.calibrated_temperature >= TEMP_HIGH:
        animations['temperature'] = temperature_animations[HIGH]
    else:
        animations['temperature'] = temperature_animations[NORMAL]

    if readings.dewpoint <= DEWPOINT_LOW:
        animations['humidity'] = humidity_animations[LOW]
    elif readings.dewpoint >= DEWPOINT_HIGH:
        animations['humidity'] = humidity_animations[HIGH]
    else:
        animations['humidity'] = humidity_animations[NORMAL]

    if readings.pressure <= PRESSURE_LOW:
        animations['pressure'] = pressure_animations[LOW]
    elif readings.pressure >= PRESSURE_HIGH:
        animations['pressure'] = pressure_animations[HIGH]
    else:
        animations['pressure'] = pressure_animations[NORMAL]

    if readings.light <= LIGHT_LOW:
        animations['light'] = light_animations[LOW]
    elif readings.light >= LIGHT_HIGH:
        animations['light'] = light_animations[HIGH]
    else:
        animations['light'] = light_animations[NORMAL]

# ANIMATED ICONS

# There are initially no animations set for the sensors because no readings
# have yet been taken

animations = { 'temperature': [], 'humidity': [], 'pressure': [], 'light': [] }

# Always yield the next animation to perform

def animation_generator():
    while True:
        for sensor_name, animation in animations.items():
            yield animation

animation = animation_generator()

def animate(animation):
    for frame in animation:
        hat.set_pixels(frame)
        sleep(FRAMETIME)
    sleep(LINGERTIME)

# READ SENSORS

def next_sensor_read_time():
    return time() + SENSOR_READ_INTERVAL

# Read the sensors in the future
# This initial delay allows the camera sufficient time to
# adjust its gain and exposure speed for meaninful results
sensor_read_time = next_sensor_read_time()

def sensor_read_time_expired():
    return time() > sensor_read_time

def advance_sensor_read_time():
    global sensor_read_time
    sensor_read_time = next_sensor_read_time()

# MAIN PROGRAMME

# Open a file for logging data
# Use a unique file name based on the current date and time
# and buffer each line (flush to disk after each line)
try:
    csv_file_name = datetime.datetime.now().strftime("comfort-%Y%m%d-%H%M%S.csv")
    f = open(csv_file_name, 'w', 1)
except IOError:
    f = sys.stdout

# Write the header line to the data file so we know the column meanings
SensorReadings.write_csv_header(f)

def loop_check(hat, cam, f):
    # Check to see if it's time to read the sensors
    if sensor_read_time_expired():
        # read the sensors again
        read_sensors(hat, cam, f)
        # set to check in the future
        advance_sensor_read_time()

    # Animate the next animation
    animate(next(animation))

try:
    while True:
        loop_check(hat, cam, f)

except KeyboardInterrupt:
    hat.clear()
    cam.close()
    sys.exit()
