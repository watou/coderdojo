import png
from os import listdir
from os.path import isfile

def chunker(seq, size):
	return (seq[pos:pos + size] for pos in range(0, len(seq), size))

imagefiles = [ f for f in listdir('.') if isfile(f) and f.endswith(".png") ]

images = []

for file in imagefiles:
	r = png.Reader(file);
	l = r.asRGB8()
	pixels = []
	for k in r.iterboxed(l[2]):
		for pixel in chunker(k,3):
			pixels.append(tuple(pixel))
	images.append(pixels)

print("images =", images)
