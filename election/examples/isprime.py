number = 17
prime = True

for i in range(2,number):
	if number % i == 0:
		prime = False

print(prime and "prime" or "not prime")


real_years = input("How old is your dog in real years? ")
print("Your dog is " + str(real_years) + " years old in real years, but " + str(float(real_years) * 7) + " years old in dog years.")

scores_string = '54 - Alice,35 - Bob,27 - Carol,27 - Chuck,05 - Craig,30 - Dan,27 - Erin,77 - Eve,14 - Fay,20 - Frank,48 - Grace,61 - Heidi,03 - Judy,28 - Mallory,05 - Olivia,44 - Oscar,34 - Peggy,30 - Sybil,82 - Trent,75 - Trudy,92 - Victor,37 - Walter'
scores_map = {int(score): student for (score,student) in [i.split(' - ') for i in scores_string.split(',')] }
for score in reversed(sorted(scores_map)):
    print("%02d: %s" % (score, scores_map[score]))


verse = """
The WHAT on the bus go HOW,
HOW, HOW,
The WHAT on the bus go HOW,
All day long.
"""

# "WHAT": "HOW"
original = 'wheels'
replacements = { 'wheels': 'round and round',
				 'horn': 'beep, beep, beep',
				 'wipers': 'swish, swish, swish',
				 'people': 'up and down',
				 'fares': 'up and up' }

# Any singular "what" nouns, otherwise assume plural
singular = ['horn']

def repl(what, how):
	localverse = what in singular and verse.replace('go', 'goes') or verse
	return '\n'.join([l.capitalize() for l in localverse.replace('WHAT', what).replace('HOW', how).split('\n')])

for what, how in replacements.items():
	print(repl(original, replacements[original]))
	print(repl(what, how))
