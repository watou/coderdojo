import os
from random import choice, randint

# Just on John's laptop
os.chdir('../src/coderdojo/election/examples/1.usernames')

# lists of words, initially empty
adjectives = []
nouns = []

#################################################
# loadWords() is a function that reads adjectives
# and nouns from simple text files containing
# words on separate lines.  The words are read
# into Python lists we can later look through.
#################################################
def loadWords():
    global nouns, adjectives
    adjectives = []
    with open('adjectives.txt') as f:
        adjectives = f.readlines()
    adjectives = [x.strip() for x in adjectives]
    nouns = []
    with open('nouns.txt') as f:
        nouns = f.readlines()
    nouns = [x.strip() for x in nouns]

#################################################
# getNewUserName() returns a new user name to the
# caller.
#################################################
def getNewUserName():
    first_word = choice(adjectives)
    second_word = choice(nouns)
    first_digit = str(randint(0, 9))
    second_digit = str(randint(0, 9))

    name = first_word + second_word + first_digit + second_digit
    return name

loadWords()
print('User name is ' + getNewUserName())
print('User name is ' + getNewUserName())
print('User name is ' + getNewUserName())