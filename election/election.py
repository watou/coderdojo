import json
import os
import fnmatch

# On John's laptop
# os.chdir('../src/coderdojo/election')

# We represent the entire Election as a class to orchestrate the
# vote collection and counting process.
#
# class variables:
# CANDIDATES = the set of candidates who are running for office
# SEATS_AVAILABLE = the number of elected offices that are open
#
# member variables:
# ballotBoxes = a list of BallotBoxes
# totalBallots = count of all Ballots across all BallotBoxes
# spoiledBallots = count of spoiled Ballots across all BallotBoxes

class Election:

    # Who are the candidates on the ballot paper running for election?
    CANDIDATES = set([
        'Oisin',
        'Marie',
        'Cian',
        'Claire',
        'Tricia',
        ])
    # set([
    #     'WILLAMENA BLENDER',
    #     'BRIDIE CHIPBUDDY',
    #     'GIANT ANTLERS',
    #     'GERALDINE GUMBY',
    #     'TODD UNCTUOUS',
    #     'HAMISH BURGLER',
    #     'TORONTO PICKLES'
    #     ])

    # How many seats or positions are available to be filled?
    SEATS_AVAILABLE = 3

    # Think of this as a game of musical chairs, where there are seven players
    # but only three chairs.

    def __init__(self):
        """
        When we create an Election, we gather up all the ballots
        across all ballot boxes.  Each ballot box is a file ending
        in .box that contains ballots.
        """
        self.ballotBoxes = []
        boxnames = fnmatch.filter(os.listdir('.'), '*.box')
        for boxname in boxnames:
            with open(boxname) as f:
                lists = json.load(f)
                box = BallotBox(boxname)
                for ballotList in lists:
                    box.addBallot(Ballot(ballotList))
                self.ballotBoxes.append(box)

        """
        Each ballot box is opened separately and the ballot papers
        in each box are counted and compared with the total number
        of ballot papers issued for that box - this is done to check
        that ballot papers have not been put into or taken out of
        the box since the poll closed. The number of ballot papers
        in each box are added together to determine the total poll.
        Individual ballot papers are not examined at this stage but
        the "tallymen" note the first preference on each ballot paper
        as it is counted.
        """
        self.totalBallots = 0
        self.spoiledBallots = 0
        for box in self.ballotBoxes:
            self.totalBallots += box.totalBallots()
            self.spoiledBallots += box.spoiledBallots()
        print('Election will include {totalBoxes} ballot boxes, which contain {totalBallots} ballots of which {spoiledBallots} are spoiled, leaving {validBallots} valid ballots to count:'
            .format(totalBoxes=len(self.ballotBoxes),
                totalBallots=self.totalBallots,
                spoiledBallots=self.spoiledBallots,
                validBallots=(self.totalBallots-self.spoiledBallots)))
        for box in self.ballotBoxes:
            box.print()


    def count(self):
        """
        All the ballot papers are mixed and then sorted according to first
        preferences, setting aside the invalid papers. The quota is then
        calculated. This is the minimum number of valid votes each successful
        candidate must get to be elected. The quota is calculated as the
        minimum number of votes, which will fill the seats available and no
        more. For example, in a three-seat constituency, the quota is a quarter
        of the valid votes, plus one - only three candidates can get this number
        of votes. In a four-seater, the quota is a fifth of the valid votes,
        plus one, and so on.

        The formula for calculating the quota is: divide the valid votes by the
        number of seats plus one, ignore any fraction and add one. As an example,
        if there are 1,000 valid votes and 4 seats, the quota is calculated as
        follows:

        1,000
        ------ + 1 = 201
        4 + 1

        This is the lowest number of votes which four candidates can obtain (804)
        but five cannot (1,005).

        The second and subsequent counts at a PR-STV election involve either the
        distribution of the surplus of an elected candidate or exclusion of the
        lowest candidate(s) and distribution of his/her/their votes.
        """
        validBallots = []
        for box in self.ballotBoxes:
            validBallots.extend([ballot for ballot in box.ballots if not ballot.isSpoiled()])
        self.quota = int(len(validBallots) / (Election.SEATS_AVAILABLE + 1)) + 1
        print('There are {valid} total valid ballots across {seats} seats, yielding a quota of {quota}'.format(
            valid=len(validBallots),
            seats=Election.SEATS_AVAILABLE,
            quota=self.quota))

        # At the start, all candidates are hopeful and have no votes
        self.hopeful = {}
        for candidate in Election.CANDIDATES:
            self.hopeful[candidate] = {}
            self.hopeful[candidate]['votes'] = []
        self.elected = {}
        self.elimntd = {}

        # Starting vote counting at round 1 and with all valid ballots,
        # count round after round until we have used up all the ballots
        round = 1
        ballots = validBallots
        while self.hopeful:
            ballots = self.countRound(round, ballots)
            round += 1

    def countRound(self, round, ballots):
        """
        Any candidate whose first preferences equal or exceed the quota is deemed
        elected. The first count is generally the only time the votes of all
        candidates are examined and sorted.
        """
        for candidate in self.hopeful:
            # Separate the ballots by candidate at the round we're examining
            self.hopeful[candidate]['votes'].extend([ballot for ballot in ballots \
                if round in ballot.choices and ballot.choices[round] == candidate])
        # An empty list signals that no surplus was distributed
        surplus = []
        for candidate in self.hopeful:
            # see if this candidate met or exceeded the quota and is elected
            if len(self.hopeful[candidate]['votes']) >= self.quota:
                # surplus ballots are gathered differently when a candidate
                # is elected in the first round of counting vs. later rounds
                if round == 1:
                    # "initial surplus"
                    # examine all of the winner's ballots and select ballots
                    # in excess of the quota to distribute to remaining
                    # candidates proportional to later preferences the
                    # following code is NOT THAT and needs to be written!
                    votes = self.hopeful[candidate]['votes'][:self.quota]
                    surplus = self.hopeful[candidate]['votes'][self.quota:]
                else:
                    votes = self.hopeful[candidate]['votes'][:self.quota]
                    # "secondary surplus" are all the ballots surplus of the quota
                    surplus = self.hopeful[candidate]['votes'][self.quota:]
                # Retain information about the winner:
                # - round: which round of the count elected
                # - votes (ELECTED): the set of ballots used to elect
                # - surplus: the extra ballots not used to elect
                self.elected[candidate] = {
                    'round': round,
                    'votes (ELECTED)': votes,
                    'surplus': surplus }
                # remove the counted ballots used by the quota, considering only the quota number of votes
                ballots = [ballot for ballot in ballots if ballot not in votes]
            else:
                # this candidate remains hopeful
                self.hopeful[candidate]['round'] = round
        # If no surplus is distributed, eliminate the lowest candidate
        victim = None
        if not surplus:
            for candidate in self.hopeful:
                if victim == None or len(self.hopeful[candidate]['votes']) < len(self.hopeful[victim]['votes']):
                    victim = candidate
        if victim:
            self.elimntd[victim] = {
                'round': round,
                'votes (ELIMINATED)': self.hopeful[victim]['votes']
            }
        # remove any elected or eliminated candidates from the list of hopefuls
        for candidate in self.elected:
            if candidate in self.hopeful:
                del self.hopeful[candidate]
        for candidate in self.elimntd:
            if candidate in self.hopeful:
                del self.hopeful[candidate]

        # If there is only one seat remaining and one hopeful candidate...
        if (Election.SEATS_AVAILABLE - len(self.elected)) == 1 and len(self.hopeful) == 1:
            # Get the name of this last hopeful candidate
            candidate = list(self.hopeful)[0]
            self.elected[candidate] = {
                'round': round,
                'votes (ELECTED)': self.hopeful[candidate]['votes'] }
            # Remove the last-elected's ballots from further consideration
            ballots = [ballot for ballot in ballots if ballot not in self.hopeful[candidate]['votes']]
            # Remove the last-elected from further consideration
            del self.hopeful[candidate]

        self.printRound(round)
        return ballots

    def printRound(self, round):
        print('After round', round)
        for candidate in self.elected:
            print('  ', candidate)
            for (k,v) in self.elected[candidate].items():
                print('    ', isinstance(v, list) and len(v) or v, k)
        for candidate in self.elimntd:
            print('  ', candidate)
            for (k,v) in self.elimntd[candidate].items():
                print('    ', isinstance(v, list) and len(v) or v, k)
        for candidate in self.hopeful:
            print('  ', candidate)
            for (k,v) in self.hopeful[candidate].items():
                print('    ', isinstance(v, list) and len(v) or v, k)


# A BallotBox holds Ballots, just like in the real world
# Our ballot boxes have names associated with each presiding election
# officer.
class BallotBox:
    def __init__(self, name):
        self.name = name
        self.ballots = []

    # Add a ballot to this ballot box, even if it's spoiled
    def addBallot(self, ballot):
        self.ballots.append(ballot)

    # Return how many ballots are in this ballot box.
    def totalBallots(self):
        return len(self.ballots)

    # Return the count of spoiled ballots in this ballot box.
    def spoiledBallots(self):
        return sum(1 for ballot in self.ballots if ballot.isSpoiled())

    def print(self):
        print('Box {name} contains {total} total ballots, of which {spoiled} are spoiled:'.format(name=self.name, total=self.totalBallots(), spoiled=self.spoiledBallots()))
        #for ballot in self.ballots:
        #    ballot.print()

# A Ballot is the Python version of the paper ballot.
#
# It contains a list of names and the number the voter put beside the name
# A Ballot could be spoiled if a number was skipped, a number is larger than
# the total number of candidates on the ballot, or if a number was used more
# than once, or if a number can't be read -- just not possible to feel confident
# that a scribble is a number.

class Ballot:
    def __init__(self, initChoices):
        self.spoiledReason = None
        self.choices = {}

        # Assign ranking to a candidate on this ballot.
        # This is the same as marking a number in the box next to a candidate
        # If the ballot already has a candidate marked with that number, that
        # means this ballot is spoilt since that's not allowed.
        for ranking, name in initChoices:
            if ranking in self.choices:
                self.spoiledReason = 'Tried to add {name} as choice #{ranking} but {nameOnList} already that choice'.format(name=name, ranking=ranking, nameOnList=self.choices[ranking])
            else:
                self.choices[ranking] = name

    def print(self):
        print('BALLOT   Problem:', self.spoiledReason)
        for ranking in sorted(self.choices.keys()):
            print('{ranking:2d}. {name}'.format(ranking=ranking, name=self.choices[ranking]))

    #
    # Make sure the voter followed the instructions for entering numbers in
    # boxes, otherwise this ballot is considered spoiled.
    #
    def isSpoiled(self):
        # If we already know this ballot is spoiled, just return that fact
        if self.spoiledReason is not None:
            return True
        # The ballot must not be blank (no preferences given)
        if len(self.choices) == 0:
            self.spoiledReason = 'The ballot must not be blank (no preferences given)'
            return True
        # The ballot must not have more candidates than are running for office
        # as found on the official list of candidate names (called CANDIDATES)
        if len(self.choices) > len(Election.CANDIDATES):
            self.spoiledReason = 'The ballot must not have more candidates than are running for office'
            return True
        # Make sure only candidates running for office appear on
        # the ballot.  So look at each candidate's name, and if
        # the name is not on the official list of candidate names (called
        # CANDIDATES), then this is a spoiled ballot.  In other words,
        # you can't "write in" a candidate!
        for candidate in self.choices.values():
            if candidate not in Election.CANDIDATES:
                self.spoiledReason = 'Only candidates running for office may appear on the ballot but someone named {name} appeared on this ballot'.format(name=candidate)
                return True
        # For as many candidates as were given a preference on the ballot,
        # make sure there is every distinct number 1, 2, etc.
        for i in range(1,len(self.choices)+1):
            if i not in self.choices:
                self.spoiledReason = 'Among {numChoices} choices, the required preference #{index} is missing'.format(numChoices=len(self.choices), index=i)
                return True
        return False

def main():
    election = Election()
    election.count()

if __name__ == '__main__':
    main()