#
import csv

def convert_csv(filename):
    with open(filename + 'csv', newline='') as incsv:
        reader = csv.DictReader(incsv)
        with open(filename + 'box', 'w', newline='') as out:
            fieldnames = None
            out.write('[\n')
            for row in reader:
                # write the CSV file to the output file
                if not fieldnames:
                    fieldnames = list(reader.fieldnames)
                else:
                    out.write(',')
                out.write('[')
                out.write(','.join(['[{0}, "{1}"]'
                    .format(row[field], field) for field in fieldnames if field != 'SN' and row[field]]))
                out.write(']\n')
            out.write(']\n')

# We were sent back two separate data files from the ISS

convert_csv('colourparties.')