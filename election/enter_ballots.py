from election import Election
from guizero import App, Text, TextBox, PushButton
import json

ballots = []

def clearPreferences():
    for candidate in preferences:
        preferences[candidate].value = ''

def addBallot():
    ballot = []
    for candidate in preferences:
        if preferences[candidate].value:
            ballot.append((preferences[candidate].value,candidate))
    global ballots
    ballots.append(ballot)
    clearPreferences()

def saveBallots():
    with open('rename_ballots.box', 'w') as out:
        global ballots
        json.dump(ballots, out)
    clearPreferences()

app = App(title='Enter a Ballot', layout='grid')
intro = Text(app, text='Enter the preferences the voter entered on his or ballot sheet.', grid=[0,0,2,1])
row = 1
preferences = {}
for candidate in Election.CANDIDATES:
    Text(app, text=candidate, grid=[0,row], align='left')
    preferences[candidate] = TextBox(app, text='', grid=[1,row])
    row += 1

ok = PushButton(app, text='Add', grid=[0,row], command=addBallot)
save = PushButton(app, text='Save', grid=[1,row], command=saveBallots)

app.display()